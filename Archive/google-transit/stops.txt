stop_id,stop_name,stop_lat,stop_lon
S1,Kmart Madison St.,36.50663464,-87.27246059
S2,Walmart Sango,36.50877779,-87.27099648
S3,2235 Madison St. & Memorial Dr.,36.50908952,-87.27467852
S4,2215 Madison St. & Memorial Dr.,36.50985807,-87.27750004
S5,Clarksville Gas Dpt. & Memorial Dr.,36.51156927,-87.27920619
S6,Null Richview Rd. & Memorial Dr.,36.51769488,-87.27820297
S7,2183 Memorial Dr. & Madison St.,36.52168242,-87.27988968
S8,302 Memorial Dr. & Madison St.,36.52197829,-87.28375600
S9,2113 Memorial Dr. & Madison St.,36.52248465,-87.28992063
S10,2133 Memorial Dr. & Madison St.,36.52223798,-87.28653090
S11,364 Memorial Dr. & Madison St.,36.52227242,-87.29668369
S12,303 Memorial Dr. & Madison St.,36.52235195,-87.29364953
S13,1955 Memorial Dr. & Madison St.,36.52240668,-87.29954623
S14,1865 Memorial Dr. & Madison St.,36.52175249,-87.30194613
S15,100 Memoiral Dr. & Madison St.,36.52052921,-87.30436765
S16,101 Memorial Dr. & Madison St.,36.51972294,-87.30635475
S17,1829 Memorial Dr. & Madison St.,36.51931136,-87.30767566
S18,1965 Memorial Dr. & Madison St.,36.51815673,-87.30926746
S19,1739 Memorial Dr. & Madison St.,36.51706458,-87.31056603
S20,1771 Memorial Dr. & Madison St.,36.51559101,-87.31176734
S21,Haynes St. & Memorial Dr.,36.51480302,-87.31383898
S22,1763 Madison St.& Veterans Plaza,36.51444506,-87.31541421
S23,Null Memorial Dr. & Veterans Plaza,36.51515017,-87.31977986
S24,1517 Madison St.& Veterans Plaza,36.51735144,-87.32480376
S25,1501- 1150 Madison St.& Veterans Plaza,36.51878844,-87.32773976
S26,1493 Madison. & Veterans Plaza,36.51927187,-87.32876940
S27,1485-148 Madison St. & Veterans Plazy,36.52095138,-87.33056068
S28,1416 Madison St. & Veterans Plaza,36.52071234,-87.33322025
S29,Veteran's Plaza,36.52112715,-87.33900134
S30,Veterans Plaza,36.52033564,-87.34078916
S31,930 Greenwood Ave. & Liberty Pkwy & Madison St.,36.52530837,-87.34822706
S32,516 Madison St. & Transit Center,36.52622201,-87.35339270
S33,137 S 3rd St. S. 2nd St. & Crossland Ave.,36.52653577,-87.35839458
S34,319 S 3rd St. S. 2nd St. & Crossland Ave.,36.52577556,-87.35813324
S35,312 Madison & Transit Center,36.52549324,-87.35646719
S36,400- 480 Union St. & Crossland Ave.,36.52522715,-87.35519238
S37,403- 411 Madison St. S. 2nd St & Crossland St.,36.52587701,-87.35532968
S38,319 Union St. S. 2nd St. & Crossland Ave.,36.52446624,-87.35664707
S39,432 2nd St. S. 2nd St. & Crossland Ave.,36.52332933,-87.35774170
S40,S. 2nd st.& Crossland Av.,36.52137397,-87.35779552
S41,180 Crossland Ave. Liberty Pkwy & Madison St.,36.52082506,-87.35597968
S42,Null Crossland Ave. & Liberty Pkwy & madison St.,36.52037661,-87.35305206
S43,Null Crossland Ave. & Liberty Pkwy & Madison St.,36.52017951,-87.35077674
S44,365 Greenwood Ave. & Liberty Pkwy & Madison St.,36.52037721,-87.34732758
S45,Null Greenwood Ave. & Liberty Pkwy& Madison St.,36.52140346,-87.34756341
S46,515 Greenwood Ave. & Liberty Pkwy & Madison St.,36.52301045,-87.34791741
S47,904 Madison St. & Liberty Pkwy & Madion St.,36.52645245,-87.34777390
S48,920 Madison St. & Liberty Pkwy & Madison St.,36.52636575,-87.34707127
S49,1144 Madison St. & Liberty Pkwy & Madison St.,36.52498480,-87.34318270
S50,1264 Madison St. & Liberty Pkwy & Madison St.,36.52349316,-87.34090204
S51,1310 Pageant Ln. & Transit Center,36.52161733,-87.33933608
S52,1300 Pageant Ln. & Liberty Pkwy & Madison St.,36.52245739,-87.33919413
S53,1340- 1135 Madison St. & Liberty Pkwy & LMadison St.,36.52165819,-87.33770093
S54,1430 Madison St. & Liberty Pkwy & Madison St.,36.52077556,-87.33466424
S55,Madison St. & Liberty Pkwy & Madison St.,36.52058010,-87.33320417
S56,1480 Madison St. & Liberty Pkwy & Madison St.,36.52003442,-87.33109708
S57,1492 Madison St. & Liberty Pkwy & Madison St.,36.51920197,-87.32926178
S58,372 Davis Dr. & K-Mart Madison St.,36.51769605,-87.32942106
S59,1498 Golf Club Ln. & K-Mart Madison St.,36.51635429,-87.32840157
S60,1514 Golf Club Ln. & K-Mart Madison St.,36.51435845,-87.32449572
S61,756 Golf Club Ln. & K-Mart Madison St.,36.51310842,-87.32351203
S62,1614 Golf Club Ln. & K-Mart Madison St.,36.51248613,-87.32267879
S63,403 Golf Club Ln. & K-Mart Madison St.,36.51182280,-87.32047679
S64,1640 Golf Club Ln. & K-Mart Madison St.,36.51167315,-87.31795352
S65,1662 Golf Club Ln. & K-Mart Madison St.,36.51238762,-87.31624009
S66,2116 Golf Club Ln. & K-Mart Madison St.,36.51310644,-87.31368641
S67,1816 Madison St. & K-Mart Madison St.,36.51388329,-87.31024821
S68,1834 Madison St. & K-Mart Madison St.,36.51400377,-87.30846694
S69,114 Madison St. & K-Mart Madison St.,36.51420391,-87.30514636
S70,1906 Madison St. & K-Mart Madison St.,36.51436574,-87.30252915
S71,1920 Madison St. & K-Mart Madison St.,36.51447809,-87.30061684
S72,1940 Madison St. & K-Mart Madison St.,36.51427333,-87.29777689
S73,1956- 196 Madison St. & K-Mart Madison St.,36.51382392,-87.29552445
S74,1990 Madison St. & K-Mart Madison St.,36.51336435,-87.29357523
S75,Dogwood Ln. Madison St. & K-Mart Madison St.,36.51293690,-87.29174447
S76,Thayer Ln. Madison St. & K-Mart Madison St.,36.51232801,-87.28915123
S77,218 Madison St. & K-Mart Madison St.,36.51019142,-87.28007143
S78,2204 Madison St. & K-Mart Madison St.,36.50987252,-87.27872492
S79,2224 Madison St. & K-Mart Madison St.,36.50931495,-87.27663461
S80,2278 Madison St. & K-Mart Madison St.,36.50885161,-87.27456826
S81,853 Glendale Dr. & Pageant Ln.,36.50403577,-87.32284595
S82,825 Glendrale Dr. & Pageant Ln.,36.50613057,-87.32243599
S83,1437 Golfview Pl. & Pageant Ln.,36.50704800,-87.32682642
S84,807 Golfview Pl. & Pageant Ln.,36.50862510,-87.32658928
S85,1340 Paradise Hill Rd. & Veterans Plaza Pageant Ln.,36.51203872,-87.33034054
S86,8008 E. Happy Hollow Dr. & Pageant Ln.,36.51236619,-87.33197190
S87,1345 Mossrose Rd. & Pageant Ln.,36.50962743,-87.33478457
S88,1300 Mossrose Rd. & Pageant Ln.,36.50982409,-87.33658627
S89,1300 W. Happy Hollow Dr. & Pageant Ln.,36.51117045,-87.33763925
S90,1279 Daniel St. & Pageant Ln.,36.51313488,-87.33851270
S91,1247 Daniel St. & Pageant Ln.,36.51335973,-87.34056206
S92,1215 Daniel St. & Pageant Ln.,36.51347427,-87.34220240
S93,11030 Daniel St. & Pageant Ln.,36.51361970,-87.34417734
S94,847 Woodmont Blvd. & Transit Center,36.51351421,-87.34750873
S95,711 Woodmont Blvd. & Transit Center,36.51316293,-87.35198855
S96,900 Cumberland Dr. & Transit Center,36.51266150,-87.35286838
S97,923 Swift Dr. & Transit Center,36.51151914,-87.35279073
S98,960 Swift Dr. & Transit Center,36.50848329,-87.35196512
S99,1001 Monroe St. & Transit Center,36.50703061,-87.35185885
S100,718 Monroe St. & Transit Center,36.50683415,-87.35026020
S101,732 Monroe St. & Transit Center,36.50666829,-87.34856628
S102,1000 Monroe St. & Transit Center,36.50651626,-87.34694877
S103,762 Monroe St. & Transit Center,36.50661438,-87.34501909
S104,106 Ashland City Rd. & Transit Center,36.50607846,-87.34463632
S105,Null Edmondson Ferry Rd. & Transit Center,36.50535929,-87.34496013
S106,126 Edmondson Ferry Rd. & Transit Center,36.50430466,-87.34524066
S107,150 Edmondson Ferry Rd. & Transit Center,36.50298971,-87.34637837
S108,192 Edmondson Ferry Rd. & Transit Center,36.50263705,-87.34772300
S109,1379 Ashland City Rd. & Tansit Center,36.50277194,-87.34970889
S110,Null Ashland City Rd. & Transit Center,36.50447746,-87.35515156
S111,Clarksville Sq. SCBig Lots & Transit Center,36.50649883,-87.36424597
S112,1055 Ashland City Rd. & Transit Center,36.50944418,-87.36637624
S113,Clarksville SC Big Lots & Greenwood Dr.,36.51093079,-87.36516827
S114,987- 989 S Riverside Dr. & Transit Center,36.51448207,-87.36598063
S115,901 S Riverside Dr. & Transit Center,36.51592340,-87.36507029
S116,715 S Riverside Dr. & Transit Center,36.51801401,-87.36343562
S117,601 S Riverside Dr. & Transit Center,36.51990953,-87.36310394
S118,599- 583 S Riverside Dr. & Transit Center,36.52170265,-87.36332500
S119,209 S Riverside Dr. & Transit Center,36.52480301,-87.36324939
S120,109- 115 S Riverside Dr. & Transit Center,36.52727255,-87.36358610
S121,419 Riverside Dr. & Transit Center,36.53135087,-87.36459811
S122,640 Riverside Dr. Providence Blvd. & Peachers Mill Rd.,36.53329149,-87.36582037
S123,621 Riverside Dr. & Transit Center,36.53471194,-87.36671414
S124,664 Riverside Dr. Providence Blvd. & Peachers Mill Rd.,36.53677091,-87.36751378
S125,792 N 2nd St. & Trainsit Center,36.54007412,-87.36493134
S126,726 N 2nd St. & Trainsit Center,36.53734636,-87.36240194
S127,524 W 2nd St. & Transit Center,36.53450857,-87.36106705
S128,404 W 2nd St. & Transit Center,36.53181861,-87.36019651
S129,220 N 2nd St. & Trainsit Center,36.52967489,-87.35945035
S130,130 Commerce St. & Greenwood Av.,36.52597723,-87.36016716
S131,65 Commerce St. & Greenwood Av.,36.52560691,-87.36182066
S132,602-604 S Riverside Dr. & Greenwood Av.,36.52006721,-87.36327036
S133,804 S Riverside Dr. & Greenwood Av.,36.51750840,-87.36383403
S134,Null S. Riverside Dr. & Greenwood Av.,36.51659066,-87.36456033
S135,1018 S Riverside Dr. & Greenwood Av.,36.51349216,-87.36658132
S136,1044 S Riverside Dr. & Greenwood Av.,36.51187164,-87.36667246
S137,1068 S Riverside Dr. & Greenwood Av.,36.51022646,-87.36657177
S138,Null Ashland City Rd. & Greenwood Av.,36.50638921,-87.36421844
S139,1192 Ashland City Rd. & Greenwood Av.,36.50579411,-87.36190671
S140,1376 Ashland City Rd. & Greenwood Av.,36.50246358,-87.34924830
S141,1409 Edmondson Ferry Rd. & Greenwood Av.,36.50262392,-87.34734036
S142,135 Edmondson Ferry Rd. & Greenwood Av.,36.50380688,-87.34540314
S143,131 Edmondson Ferry Rd. & Greenwood Av.,36.50420114,-87.34521980
S144,123 Edmondson Ferry Rd. & Greenwood Av.,36.50487090,-87.34507671
S145,107 Edmondson Ferry Rd. & Greenwood Av.,36.50591213,-87.34467729
S146,Greenwood Cemetery & Greenwood Av.,36.50672975,-87.34407102
S147,893 Greenwood Av. & Veterans Plaza,36.51400590,-87.34626293
S148,Five Points & Greenwood Av.,36.51555696,-87.34699555
S149,801 Greenwood Av. & Veterans Plaza,36.51675467,-87.34691420
S150,Null Richardson St. & Veterans Plaza,36.51795514,-87.34233614
S151,901 Greenwood Av. & Vererans Plaza,36.51340131,-87.34581382
S152,1340- 135 Pegeant Ln. & South Central Village,36.51860269,-87.33941188
S153,Null Pageant Ln. & South Central Village,36.51748906,-87.33964791
S154,Pageant Ln. & Paradise Hill Rd.,36.51622680,-87.33904867
S155,Transit Center (Admin Bldg) - 430 Boillin Ln,36.51603002,-87.33460888
S156,1274 Pradise Hill Rd. & Greenwood Av.,36.51443313,-87.33477275
S157,Null Paradise Hill Rd. & Greenwood Av.,36.51396521,-87.33368222
S158,19290 Paridise Hill Rd. & Greenwood Av.,36.51331709,-87.33255193
S159,South Central Village,36.50937101,-87.32876683
S160,1420 South Central Villages Apt.,36.50884000,-87.32775142
S161,1594 Vista Ln. & South Central Village,36.50838771,-87.32037811
S162,1580 Vista Ln. & South Central Village,36.50663452,-87.32053686
S163,1801 Vista Ln. & South Central Village,36.50383334,-87.32108002
S164,Transit Center,36.527978001,-87.358472001
S165,426 College St. Providence Blvd. & Peachers Mill Rd.,36.53089831,-87.35595937
S166,524 College St. Providence Blvd. & Peachers Mill Rd.,36.53124954,-87.35408557
S167,722 College St. Providence Blvd. & Peachers Mill Rd.,36.53182824,-87.35143594
S168,812 College St. Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.53230241,-87.34936316
S169,900 College St. Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.53271859,-87.34741557
S170,1114 College St. Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.53344414,-87.34458183
S171,1158 College St.  Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.53505409,-87.34092729
S172,1306- 1320 College St. Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.53817526,-87.33765688
S173,1330 College St. Dumbar Cave Rd & Wilma Rudolph Blvd.,36.54060618,-87.33716688
S174,1549 Wilma Rudolph Blvd.  Dumbar Cave Rd & Willma Rudolph Blvd.,36.54773760,-87.33267673
S175,1655 Wilma Rudolph Blvd.  Dumbar Cave Rd. & Wilma Rudolph Blvd.,36.55000390,-87.32972833
S176,Null Wilma Rudolph Blvd.  Dumbar Cave Rd. & wilma Rudolph Blvd.,36.55207466,-87.32701229
S177,1801 Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.55437306,-87.32392539
S178,1891 Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.55591514,-87.32183548
S179,2021 Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.55885513,-87.31786496
S180,2081 Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.56209350,-87.31356645
S181,2031 Wilma Rudolph Blvd. & Kmart Wilma RudolphBlvd. Gateway Medical Center,36.55999774,-87.31612209
S182,2147 Wilma Rudolph Blvd. & Kmart Wilma RudolphBlvd. Gateway Medical Center,36.56460271,-87.31028763
S183,2201 Wilma Rudolph Blvd. & Kmart Wilma RudolphBlvd. Gateway Medical Center,36.56643849,-87.30780416
S184,Null Wilma Ruldolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.56794048,-87.30579022
S185,2269 Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.56970585,-87.30423457
S186,Wilma Rudolph Blvd. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.57175400,-87.30338469
S187,Wyatt Johnson mazda & Kmart Wilma Rudolph Blvd.,36.57505469,-87.30221382
S188,2503 Wilam Rudolph Blvd. & Kmart Wilma Ruldolph,36.57656370,-87.30138839
S189,2531 Wilma Ruldolph Blvd. & Kmart Wilma Ruldolph,36.57762816,-87.30065917
S190,2601 Wilmal Rudolph Blvd. & Kmart Wilma Ruldolph,36.57976564,-87.29902512
S191,Wilma Ruldolph Needmore Rd. & Kmart Wilma Rudolph,36.58127710,-87.29787643
S192,2701 Wilma Rudolph Blv. & Kmart Wilma Ruldolph Blvd. Gateway Medical Center,36.58362203,-87.29610179
S193,Null Dunlop Ln. & Kmart Wilma Rudolph Blvd. Gateway Medical Center,36.58541684,-87.29500410
S194,2831 Wilma Rudolph Blvd. & Governors Square Mall Gateway Medical Center,36.58690162,-87.29418178
S195,2815 Wilma Rudolph Blvd. & Governors Square Mall Gateway Medical Center,36.58916119,-87.29281681
S196,2821 Wilma Rudolph Blvd. & Gateway Medical Center,36.59058919,-87.29166464
S197,Gov. Square Mall,36.59162686,-87.28637501
S198,Walmart Wilma Rudolph Blvd.,36.59410650,-87.28839671
S199,3075 Wilma Rudolph Blvd. & Wal-Mart Wilma Rudolph Blvd. Gateway Medical Center,36.59577254,-87.28684533
S200,3098 Westfield Ct. & Wal-Mart Wilma Ruldolph Blvd.,36.59773118,-87.28633001
S201,Wal-Mart Wilma Rudolph Blvd. & Wilma Ruldolph Blvd.,36.59808135,-87.28848531
S202,2821 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.K-Mart Wilma Rudolph Blvd.,36.59140255,-87.29120022
S203,2800 Wilma Rudolph Blvd. & W. Dunbar Cave Rd. K- Mart Wilma Rudolph Blvd.,36.58768498,-87.29399116
S204,2788 Dunlop Ln. & K-Mart Wilma Rudolph Blvd.,36.58594793,-87.29575461
S205,Kmart Wilma Rudolph Blvd.,36.58557085,-87.29820226
S206,K- Mart Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.58437632,-87.29742725
S207,2760- 76 Dunlop Ln. & Dunbar Cave Rd.,36.58402156,-87.29650434
S208,2702 Wilma Rudolph Blvd. & W. Dunbar Cave Rd. Gateway Medical Center,36.58335073,-87.29655322
S209,2654 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.58162128,-87.29787191
S210,2600 Wilma Rudolph Blvd. W. Dunbar Cave Rd.,36.57959206,-87.29941809
S211,2514 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.57791113,-87.30069981
S212,2155 Wilma Rudolph Blvd. W. Dunbar Cave Rd.,36.57601527,-87.30195268
S213,2300 Wilma Rudolph Blvd. & Center Point Dr W. Dunbar Cave Rd.,36.56883618,-87.30507688
S214,2200 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.56623162,-87.30831629
S215,2150 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.56484987,-87.31017568
S216,2090 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.56262533,-87.31320578
S217,2070 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.56141006,-87.31471829
S218,2010-12 WIlma Rudolph Blvd. & W. Dunbar Cave Rd.,36.55911203,-87.31778239
S219,1860 Wilma Rudolph Blvd. & W. Dunbar Cave Rd.,36.55621768,-87.32168691
S220,1841 Business Park Dr. & W. Dunbar Cave Rd.,36.55632337,-87.32464358
S221,121 W. Dunbar Cave Rd.  Transit Center,36.55523080,-87.32578329
S222,W. Dunbar Cave Rd. & Wilma Rudolph Blvd.,36.55384425,-87.32488226
S223,1650 Wilma Rudolph Blvd. & Transit Center College St.,36.55058348,-87.32922201
S224,1630 Wilma Rudolph Blvd. & Transit Center College St.,36.54831971,-87.33216171
S225,1361 College St. & Transit CenterHornberger Ln.,36.54091846,-87.33725917
S226,1202 Hornberger Ln. & Transit Center,36.53493467,-87.34015677
S227,1308 Frnaklin St. & College St.,36.53445803,-87.33871501
S228,1241 Reynolds St. & College St. Transit Center,36.53279451,-87.33730074
S229,1263 Cedar St. & College St. Transit Center,36.53167124,-87.33767865
S230,1141 Franklin St. & College St. Transit Center,36.53217382,-87.34277841
S231,117 Franklin St. & College St. College St.,36.53054023,-87.34587276
S232,841 Franklin St. & College St. Transit Center,36.53006488,-87.34751967
S233,709 Franklin St. & College St. College St.,36.52948282,-87.35035004
S234,634 Franklin St. & College St. Transit Center,36.52928531,-87.35126449
S235,537 Franklin St. & Transit Center,36.52883633,-87.35326514
S236,400 Franklin St. & Transit Center,36.52811092,-87.35653713
S237,309 W 2nd St. Providence Blvd. & Peachers Mill Rd.,36.53065783,-87.35964176
S238,409 W 2nd St. Providence Blvd. & Peachers Mill Rd.,36.53193750,-87.36006334
S239,200 W 2nd St. Providence Blvd. & Peachers Mill Rd.,36.53308514,-87.36045254
S240,531 N 2nd St. Providence Blvd. & Peachers Mill Rd.,36.53452646,-87.36090782
S241,705 N 2nd St. Providence Blvd. & Peachers MIll Rd.,36.53635903,-87.36161812
S242,Westgate Inn & Suites Providence Blvd. & Peachers Mill Rd.,36.54124249,-87.36687464
S243,203 Providence Blvd. & Peachers Mill Rd.,36.54577166,-87.37447879
S244,301 Providence Blvd. & Peachers Mill Rd.,36.54649641,-87.37558783
S245,Null Providence & Peachers Mill Rd.,36.54814043,-87.37812162
S246,609 Providence & Peachers Mill Rd.,36.54935831,-87.37999392
S247,649 Providence & Peachers Mill Rd.,36.55016904,-87.38229566
S248,Null Preachers Mill Rd. & Howell Rd. & Saratoga Rd.,36.55149391,-87.38238108
S249,492 Peachers Mill Rd. &  Howell Rd. & Saratoga Rd.,36.55598143,-87.38116508
S250,501 Peachers Mill Rd. &  Howell Rd. & Saratoga Rd.,36.55857822,-87.38143812
S251,367 Peachers Mill Rd. &  Howell Rd. & Saratoga Rd.,36.56000193,-87.38240651
S252,365 Spencer Ln. & Howell Rd.,36.55964542,-87.38328075
S253,308 Jackson Rd. & Howell Rd.,36.55972570,-87.38746506
S254,219 Jackson Rd. & Howell Rd.,36.55982854,-87.38913745
S255,102 Jackson Rd. & Howell Rd.,36.55996932,-87.39000233
S256,205 Jackson Rd. & Howell Rd.,36.56064602,-87.39176567
S257,223 Dellwood Ln. & Howell Rd.,36.56202817,-87.39314988
S258,11 Hillsboro Rd. & Howell Rd.,36.56395857,-87.39200311
S259,21 Hillsboro Rd. & Howell Rd.,36.56541246,-87.39176311
S260,49 Hillsboro Rd. & Howell Rd.,36.56596012,-87.38777695
S261,811-0815 Peachers Mill Rd. & Howell Rd. & Saratoga Rd.,36.57012311,-87.38600582
S262,300 Peachers Mill Rd. & Howell Rd. & Saratoga Rd.,36.57179057,-87.38640695
S263,300 Peachers Mill Rd. &  Howell Rd. & Saratoga Rd.,36.57430638,-87.38801930
S264,1001 Peachers Mill Rd. & Howell Rd. & Saratoga Rd.,36.57646490,-87.38941096
S265,301 Peachers MIll Rd. &  Howell Rd. & Saratoga Rd.,36.57740834,-87.39002190
S266,189 Pine Moutain Rd. & Howell Rd.,36.57810925,-87.39243991
S267,193 Pine Moutain Rd. & Howell Rd.,36.57796477,-87.39358506
S268,235 Pine Moutain Rd. & Howell Rd.,36.57839285,-87.39966014
S269,501 Pine Moutain Rd. & Howell Rd.,36.57859897,-87.40099189
S270,Howell Rd. & Saratoga Rd.,36.57852634,-87.40630416
S271,310 Shanon St.& Wal-Mart North,36.57723612,-87.40663575
S272,1611 Ft. Campbell Blvd & Airport Rd. Wal-Mart North,36.57629865,-87.40797936
S273,1671 Ft. Campbell Blvd & Airport Rd. Tobacco Rd. Pollard Rd. & Peachers Mill Rd. 101st Airborne Div.,36.58073185,-87.41001870
S274,1685 Ft. Campbell Blvd & Airport Rd. tobacco Rd. Pollard Rd. 101st Airborne Div.,36.58152346,-87.41037850
S275,101st Airborn Division Pky & Pollard Rd. Whitifield Rd.,36.58707709,-87.40423209
S276,1281 101st Airborn Pky & Peachers Mill Rd.,36.58630950,-87.39533997
S277,261 Stonecorssing Dr. & Peachers Mill Rd.,36.58602704,-87.39476978
S278,267 Stonecrossing Dr. & Peachers Mil Rd.,36.58587205,-87.39308175
S279,1220 Peachers Mill Rd. & Providence Blvd.,36.58488625,-87.39125863
S280,181 Peachers Mill Rd. & Providenve Blvd.,36.57879114,-87.39100177
S281,226 Peachers Mill Rd. & Providence Blvd.,36.57653862,-87.38955392
S282,298 Peachers Mill Rd. & Providence Blvd.,36.57428687,-87.38810227
S283,248 Peachers Mill Rd. & Providence Blvd.,36.57308560,-87.38731845
S284,830 Peachers Mill Rd. & Providence Blvd.,36.57030552,-87.38609029
S285,302 Bancroft Dr. & Providence Blvd.,36.56942163,-87.38537685
S286,332 Bancroft Dr. & Providence Blvd.,36.56936794,-87.38293352
S287,346 Bancroft Dr. & Providence Blvd.,36.56928384,-87.38083821
S288,514 Pollard Rd. & Providence Blvd.,36.56823972,-87.37770760
S289,490 Pollard Rd. & Providence Blvd.,36.56674460,-87.37845103
S290,482 Pollard Rd. & Providence Blvd.,36.56546081,-87.37924956
S291,462 Pollard Rd. & Providence Blvd.,36.56389572,-87.38087895
S292,593 Pollard Rd. & Providence Blvd. & Transit Center,36.55896903,-87.38164027
S293,500 Peachers Mill Rd. & Providence Blvd. & Transit Center,36.55618999,-87.38123788
S294,660 Providence Blvd. & Providence Blvd. & Transit Center,36.54992501,-87.38233490
S295,624 Providence Blvd. & Providence Blvd. & Transit Center,36.54917544,-87.37994209
S296,231 Oak St. & Transit Center,36.54757822,-87.37965868
S297,206 Oak St. & Transit Center,36.54532435,-87.37997732
S298,198 B. St. & Transit Center,36.54467863,-87.37758585
S299,346 A St. & Transit Center,36.54225447,-87.37700333
S300,206 A St. & Transit Center,36.54209184,-87.37481556
S301,Null Walkers St. & Providence Blvd. & Kraft St.,36.54257320,-87.37419637
S302,154 Walkers St. & Providence Blvd. & Kraft St.,36.54421968,-87.37386944
S303,142 Old Tn-76 & Providence Blvd.,36.54353323,-87.37293649
S304,734 Providence Blvd. & Kraft St.,36.54124135,-87.36729046
S305,809- 811 Providence Blvd. Ft. Campbell Blvd. & Airport Rd. Donna Dr. & Dover Rd.,36.55090352,-87.38627859
S306,703 Prvidence Blvd. Ft. Campbell Blvd. & Airport Rd. Donna Dr. & Dover Rd.,36.55053548,-87.38400099
S307,901 Providence Blvd. Ft. Campbell Blvd. & Airport Rd. Donna Dr. & Dover Rd.,36.55120694,-87.38793861
S308,239 Dover Rd. & Donna Dr.,36.55077158,-87.39813907
S309,313- 319 Dover Rd. & Donna Dr.,36.55050366,-87.40196765
S310,Darlene Dr. Dover Rd. & Donna Dr.,36.55042992,-87.40298090
S311,363 Dover Rd. & Donna Dr.,36.55052395,-87.40702582
S312,445 Dover Rd. & Donna Dr.,36.55148469,-87.41294906
S313,523 Dover Rd. & Donna Dr.,36.55186461,-87.41527621
S314,549 Dover Re. & Donna Dr.,36.55237306,-87.41840806
S315,608 Dover Rd. & Donna Dr.,36.55304213,-87.42254693
S316,Null Dover Rd. & Donna Dr.,36.55357164,-87.42581915
S317,Donna Dr. & Dover Rd.,36.55468357,-87.43100393
S318,571 Donna Dr. & Wal-Mart North,36.55637358,-87.43027793
S319,559 Donna Dr. & Wal-Mart North,36.55790264,-87.42996943
S320,545 Donna Dr. & Wal-Mart North,36.55980365,-87.42954308
S321,537 Donna Dr. & Wal-Mart North,36.56072636,-87.42939232
S322,421 Donna Dr. & Wal-Mart North,36.56315044,-87.42899930
S323,401 Donna Dr. & Wal-Mart North,36.56552238,-87.42863188
S324,403 Woodale Dr. & Wal-Mart North,36.56558021,-87.42752695
S325,701 R. S. Bradley Blvd. & Wal-Mart North,36.56844813,-87.42710819
S326,423 Cunningham & Wal-Mart North,36.56910157,-87.43080017
S327,378 Cunningham & Wal-Mart North,36.57273144,-87.43028145
S328,724 Cunningham & Dupuis Dr. & Charlemage Blvd.,36.57204498,-87.42693454
S329,745 Layafette Rd. & Wal-Mart North,36.57319120,-87.42828357
S330,745 Cunningham Ln. & Wal-Mart North,36.57447043,-87.42692378
S331,300 Cunningham Ln. & Wal-Mart North,36.57441314,-87.42359883
S332,218 Cunningham & Wal-Mart North,36.57439521,-87.41734983
S333,146- 166 Cunningham & Wal-Mart North,36.57438076,-87.41333063
S334,Walmart North,36.57924009,-87.41234820
S335,1646 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd. Lafayette Rd. & Cunningham Ln.,36.57733932,-87.40875161
S336,1600- 1632 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd. Lafayette Rd. & Cunningham Ln.,36.57601103,-87.40814709
S337,146-166 Cunningham Ln. & Lafayette Rd.,36.57446051,-87.41067945
S338,215 Cunningham Ln. & Lafayette Rd.,36.57446940,-87.41426913
S339,1601 Cunningham Ln. & Lafayette Rd.,36.57447687,-87.41984942
S340,1601 Cunningham Ln. & Lafayette Rd.,36.57448537,-87.42176832
S341,301 Cunningham Ln. & Lafayette Rd.,36.57448442,-87.42328375
S342,1600 Cunningham Ln. & Lafayette Rd.,36.57445279,-87.42659267
S343,646 Layafette Rd. Dupuis Dr. & Charlemange Blvd.,36.57122233,-87.42591931
S344,268 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.57018490,-87.42405885
S345,616 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56912852,-87.42164418
S346,570 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56773720,-87.41934039
S347,560 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56663481,-87.41729998
S348,516 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56542655,-87.41449495
S349,470 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56303406,-87.41198355
S350,466 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56177751,-87.41057435
S351,428 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56072653,-87.40884735
S352,5055 Lafayette Rd. Dupuis Dr. & Charlemange Blvd.,36.56001060,-87.40760060
S353,107 Charlemange Blvd. & Dupuis Dr.,36.55873189,-87.40666512
S354,400 Charlemange Blvd. & Dupuis Dr.,36.55780147,-87.40681446
S355,215 Charlemange Blvd. & Dupuis Dr.,36.55584393,-87.40710497
S356,300 Charlemange Blvd. & Dupuis Dr.,36.55467275,-87.40733152
S357,5017 Charlemange Blvd. & Dupuis Dr.,36.55374584,-87.40754321
S358,Dupuis Dr. & Charlemagne Blvd.,36.55147126,-87.40782177
S359,363 Dupuis Dr. & Transit Center,36.55118827,-87.40711123
S360,342 Dupuis Dr. & Transit Center,36.55151556,-87.40433008
S361,297 Orleans Dr. & Transit Center,36.55534815,-87.40364534
S362,206 Orleans Dr. & Transit Center,36.55696757,-87.40367488
S363,288 Lafayette Rd. & Transit Center,36.55726393,-87.40153050
S364,1172 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd. Transit Center,36.55608731,-87.39664340
S365,1128 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd. Transit Center,36.55361421,-87.39546649
S366,920- 924 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd. Transit Center,36.55110421,-87.39079702
S367,820- 924 Providence Blvd. & Peachers Mill Rd. Transit Center,36.55103871,-87.38791383
S368,804 Providence Blvd. & Peachers Mill Rd. Transit Center,36.55076465,-87.38629658
S369,712 Providence Blvd. & Peachers Mill Rd. Transit Center,36.55043209,-87.38446948
S370,500 Providence Blvd. & Kraft St.,36.54816972,-87.37842856
S371,212 Providence Blvd. & Kraft st.,36.54584542,-87.37485017
S372,601 8th St. Providence Blvd. & Peachers Mill Rd.,36.53385492,-87.35014292
S373,Burt Elementary School & Providence Blvd. & Peachers Mill Rd.,36.53629265,-87.35010541
S374,601 College St. & Transit Center,36.53840161,-87.35074536
S375,601 College St. & Transit Center,36.53858560,-87.35269191
S376,601 Drane St. & Transit Center,36.53606965,-87.35553221
S377,601 College St. & Transit Center,36.53481966,-87.35727062
S378,305 Marion St. & Transit Center,36.53439180,-87.35842602
S379,1141 Ft. Campbell Blvd. & Airport Rd.,36.55315039,-87.39496462
S380,1193 Ft. Campbell Blvd. & Airport Rd.,36.55584970,-87.39622659
S381,1250 Ft. Campbell Blvd. & Airport Rd.,36.55869600,-87.39815522
S382,1305 Ft. Campbell Blvd. & Airport Rd.,36.56103638,-87.39987867
S383,1333 Ft. Campbell Blvd. & Airport Rd.,36.56500612,-87.40274392
S384,1425 Ft. Campbell Blvd. & Airport Rd.,36.56746269,-87.40393382
S385,1501- 1507 Ft. Campbell Blvd. & Airport Rd.,36.56953923,-87.40488918
S386,1541 Ft. Campbell Blvd. & Airport Rd.,36.57244925,-87.40622953
S387,1571- 1579 Ft. Campbell Blvd. & Airport Rd.,36.57404508,-87.40695582
S388,1801 Ft. Campbell Blvd. & Airport Rd.,36.58674300,-87.41279491
S389,1887 Ft. Campbell Blvd. &  Airport Rd. Commissary,36.58855717,-87.41381278
S390,1911 Ft. Cmapbell Blvd. &  Airport Rd. Commissary,36.58978611,-87.41457854
S391,1949- 1951 Ft. Campbell Blvd. &  Airport Rd. Commissary,36.59150632,-87.41576394
S392,1991 Ft. Campbell Blvd. & Airport Rd. Commissary,36.59353335,-87.41736325
S393,2029 Ft. Campbell Blvd. &  Airport Rd. Commissary,36.59589385,-87.41917931
S394,2043 Ft. Campbell Blvd. &  Airport Rd. Comissary,36.59800878,-87.42050900
S395,2133- 2151 Ft. Campbell Blvd. &  Airport Rd. Commissary,36.60300948,-87.42364947
S396,2133- 2151 Ft. Campbell Blvd. &  Airport Rd. Commissary,36.60419952,-87.42439960
S397,2705 Ft. Campbell Blvd. & Durrett Center Commissar,36.62276880,-87.43213526
S398,2771 Ft. Campbell Blvd. & Durrett Center Commissary,36.62428526,-87.43252896
S399,2851- 2867 Ft. Campbell Blvd. & Durrett Center Commissary,36.62684848,-87.43320440
S400,3001 Ft. Campbell Blvd. & Durrett Center Commissary,36.62995159,-87.43401806
S401,3193 Ft. Campbell Blvd. & Durrett Center Commissary,36.63455929,-87.43539502
S402,3315 Ft. Campbell Blvd. & Durrett Center Commissary,36.63747667,-87.43627256
S403,3325 Ft. Campbell Blvd. & Durrett Center Commissary,36.63842409,-87.43623937
S404,3447 Ft. Compbell Blvd. & Durrett Center Commissary,36.63992799,-87.43613798
S405,2601 Ft. Campbell Blvd. & Durrett Center Commissary,36.61838804,-87.43099292
S406,Ft. Campbell Blvd. & Wal-Mart North,36.63530973,-87.43630279
S407,Ft. Campbell Blvd. & Wal-Mart North,36.62722566,-87.43355583
S408,Ft. Campbell Blvd. & Wal-Mart North,36.62494670,-87.43295146
S409,Ft. Campbell Blvd. & Wal-Mart North,36.62251721,-87.43228033
S410,Ft. Campbell Blvd. & Wal-Mart North,36.61818224,-87.43121251
S411,Ft. Campbell Blvd. & Wal-Mart North,36.61415179,-87.43068723
S412,2250 Ft. Campbell Blvd. & Wal-Mart North,36.60716994,-87.42665279
S413,2218 Ft. Campbell Blvd. & Wal-Mart North,36.60577683,-87.42568270
S414,2188 Ft. Campbell Blvd. & Wal-Mart North,36.60484137,-87.42507873
S415,2120- 2130 Ft. Campbell Blvd. & Wal-Mart North,36.60213376,-87.42337709
S416,2084 Ft. Campbell Blvd. & Wal-Mart North,36.59912343,-87.42149615
S417,2052 Ft. Campbell Blvd. & Wal-Mart North,36.59730023,-87.42035446
S418,2018 Ft. Campbell Blvd. & Wal-Mart North,36.59559371,-87.41927099
S419,201 Ft. Campbell Blvd. & Wal-Mart North,36.59466810,-87.41859069
S420,1950 Ft. Campbell Blvd. & Wal-Mart North,36.59185014,-87.41632153
S421,1986 Ft. Campbell Blvd. & Wal-Mart North,36.59338718,-87.41754217
S422,1886-1894 Ft. Campbell Blvd. & Wal-Mart North,36.58935929,-87.41458571
S423,1826 Ft. Campblell Blvd. & Wal-Mart North,36.58761753,-87.41355659
S424,1680 Ft. Campbell Blvd. & Pollard Rd. & Peachers Mill Rd.,36.58030743,-87.41010683
S425,1598 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd,36.57337544,-87.40691940
S426,1504 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56967612,-87.40521988
S427,1460 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56876038,-87.40480511
S428,1440 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56729154,-87.40412900
S429,1410- 1412 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56620319,-87.40362445
S430,1370 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56414795,-87.40245827
S431,1328 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.56136283,-87.40041080
S432,1186 Ft. Campbell Blvd. Providence Blvd. & Peachers Mill Rd.,36.55846511,-87.39827977
S433,110 Dover Crossing Rd. & Providence Blvd.,36.55271225,-87.39702096
S434,Providence Blvd. & Kraft St.,36.54093243,-87.36495159
S435,855 Kraft St & Transit Center,36.54167350,-87.36259032
S436,371 Kraft St. & Transit Cener,36.54163826,-87.35925551
S437,513 Kraft St. & Transit Center,36.54128395,-87.35517993
S438,491 Kraft St. & Transit Center,36.54076661,-87.35393760
S439,350 Kraft St. & Transit Center,36.54147128,-87.35241355
S440,Ladd Dr. & Transit Center,36.54138073,-87.35118575
S441,Ladd Dr. & Transit Center,36.54128939,-87.35029682
S442,Hassle Dr. & Transit Center,36.54055951,-87.35018246
S443,N 8th St. & Transit Center,36.54009881,-87.34952467
S444,801 N 8th St. Marioin St. & Transit Center,36.53849214,-87.34980325
S445,601 N 8th St. & Transit Center,36.53780241,-87.34991602
S446,601 College St. & Transit Center,36.53588319,-87.35022387
S447,601 College St. & Transit Center,36.53361381,-87.35032178
S448,601 College St. & Transit Center,36.53195802,-87.35147373
S449,601 College St. & Trainsit Center,36.53134706,-87.35426087
S450,College St. & Transit Center,36.53091470,-87.35631645
S451,202 Tobacco Rd. Airport Rd.,36.60492225,-87.42305286
S452,226 Tobacco Rd. Airport Rd.,36.60387378,-87.42108074
S453,242 Tobacco Rd. Ft. Campbell Blvd. Airport Rd.,36.60312581,-87.41968108
S454,303 Tobacco Rd. Ft. Campbell Blvd. Airport Rd.,36.60231373,-87.41817527
S455,699 Jack Miller Blvd. Providence & Peachers Mill Rd.,36.60650152,-87.41396329
S456,330 Jack Miller Blvd. Providence Blvd. & Peachers Mill Rd.,36.61199076,-87.41768415
S457,199 Jack Miller Blvd. Providence & Peachers Mill Rd.,36.61711432,-87.42112103
S458,Jack Miller Blvd. & Airport Rd.,36.61561092,-87.42850306
S459,311 Maket St. & Providence Blvd. & Peachers Mill Rd.,36.54617981,-87.37366905
S460,Market St. & Transit Center,36.54769709,-87.37340667
S461,202 Chapel St. Providence Blvd. & Peachers Mill Rd.,36.54807989,-87.37526496
S462,State Line Rd. & Durrett Dr. Tiny Town Rd.,36.64082151,-87.43473025
S463,State Line Rd. & Wal-Mart North,36.64090130,-87.43312586
S464,Durrett Center,36.64092342,-87.43108466
S465,State Line Rd. & Wal-Mart North,36.64095965,-87.42827646
S466,State Line Rd. & Tiny Town Rd.,36.64250615,-87.42533873
S467,State Line Rd. & Wal-Mart North,36.64353091,-87.42311583
S468,State Line Rd. & Tiny Town Rd.,36.64379659,-87.41925499
S469,Pembrook Rd. & Wal-Mart North,36.64328143,-87.41458929
S470,Pembrook Rd. & Tiny Town Rd.,36.64164798,-87.41438821
S471,602 Pembrook Rd. & Tiny Town Rd.,36.63778549,-87.41358613
S472,3398 Pembrook Rd. & Tiny Town Rd.,36.63749624,-87.41097380
S473,3395 Pembrook Rd. & Tiny Town Rd.,36.63722206,-87.40851061
S474,1201 Tobacco Rd. & Walgreen North,36.63598532,-87.40714025
S475,Tobacco & Tiny Town Rd.,36.63296790,-87.40760256
S476,901 Tiny Town Rd. & Tobacco Rd.,36.63465709,-87.40121253
S477,912 Tiny Town Rd. & Tobacco Rd.,36.62988665,-87.39937788
S478,940 Tiny Town Rd. & Tobacco Rd.,36.62955802,-87.39610712
S479,2713 Arbor St. &Airport Fd.,36.63332686,-87.42650714
S480,286 Tiny Town Rd. & Tobacco Rd.,36.63246808,-87.42968289
S481,227 Burch Rd. Ft. Campbell Blvd. & Airport Rd.,36.62961428,-87.42485380
S482,209 Burch Rd. Ft. Campbell Rd. & Airport Rd.,36.62794290,-87.42556627
S483,State Line Red & Tiny Town Rd.,36.64263439,-87.42541195
S484,2707 Arbor St. & Airport Fd.,36.63318859,-87.42473855
S485,Veterans Plaza & South Central Village,36.52121104,-87.34199447
S486,221 Madison St. & Transit Center,36.52560638,-87.35701293
S487,Avondale Dr. & Transit Center,36.50445006,-87.35551951
S488,936 Greenwood Av. & Veterans Plaza,36.51117693,-87.34533894
S489,Ft. Campbell Blvd. Airport Rd.,36.56381169,-87.40203676
S490,1103 Crossland Ave. & Liberty Pkwy,36.51947197,-87.34351939
S491,1002 Crossland Ave. & Liberty Pkwy,36.51966758,-87.34556200
S492,1026 Washington St. & Transit Center,36.52241777,-87.34875715
S493,517 Washington St. & Transit Center,36.52327746,-87.35223734
S494,Peachers Mill Rd. & Providence Blvd.,36.56417417,-87.38390795
S495,Lafayette Rd. & Cunningham Ln.,36.57429368,-87.42974711
S496,Walgreens & Tobacco Rd.,36.62473250,-87.31865231
S497,PXTRA & Gate 4,36.64483296,-87.44940917
S498,Gate 4,36.64460003,-87.43690638
S500,Hospital/Medical Center,36.58224823,-87.26975869
S501,101st Airborn Div. & Whitfield blvd,36.58077965,-87.33644998
