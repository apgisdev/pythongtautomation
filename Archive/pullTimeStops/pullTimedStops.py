# Harrison Welch
# ------------------------------------------
# DESC: a program designed to rip the esult_stops_master_index_csv.txt file up and
#   write out to a file to get all "timed stops" within the data stop_name_position
#
# NOTE: "Timed stops" are stops in CTS where the bus arrives and depars at a specific
#   time

import csv as cv

# Get the File to read
filename = "stops.txt"
readFile = open(filename,'rU')
csvFile = cv.reader(readFile,delimiter=',', quotechar='\n')

# get the file to write
writeFile = open('result_' + filename,'w')

# Build table out of the readFile
table = []
for row in csvFile:
    print str(row)
    table.append(row)

# if the last entry in the row is '1' then the line is time and we need to write
#   it out to the file
for i in range(0, len(table)):
    if table[i][len(table[i])-1] == '1':
        for j in range(0, len(table[i])):
            if j == (len(table[i]) - 1):
                writeFile.write(table[i][j])
            else:
                writeFile.write(table[i][j]+",")
        writeFile.write("\n")
