# arcmap_csv_to_stop_data.py

from tokenize import tokenize
import csv as cv

print "Hello World";

table = []
readFile = open('route1_outbound_first_last.csv','rU')

csvFile = cv.reader(readFile,delimiter=',', quotechar='\n')

for row in csvFile:
    # print str(row) + str(len(row))
    table.append(row)

writeFile = open('result_route1_outbound_first_last.csv','w')

for x in range(0,len(table)):

    stop_id = "S" + str(table[x][1])

    # decide if the stop has a CTS name, otherwise use it's address

    stop_name = table[ x ][ 6 ]

    # print stop_name

# 3,1,Point,164,http://www.ridects.com/wp-content/uploads/Clarksville-TimeSideRoutesTimes.pdf,0,214 N 3rd St./ Providence Blvd. & Peachers Mill Rd./ Walmart North,"Route 1, Route 2, Route 3, Route 4, Route 7",0,Central Time,0, , ,1569431.697,801990.7629,-87.358207,36.528908

    # lon of the current line
    lon = table[ x ][ len(table[x])-1 ]

    # lat of the current line
    lat = table[ x ][ len(table[x])-2 ]

    # combine all into a single line
    theLine = stop_id + "," + str(stop_name).translate(None, '/') + "," + lon + "," + lat

    print theLine

    # write to the file the line plus a newline
    writeFile.write(theLine + "\n");

print "Done"
