import csv as cv
import re

readFile = open("google_transit_ex_wShapes_v2/stops.txt","rU")
writeFile = open("result_stops.txt","w")

table = []

csvFile = cv.reader(readFile,delimiter=',', quotechar='\n')

for row in csvFile:
    table.append(row)

writeFile.write(table[0][0] + "," + table[0][1] + "," + table[0][2] + "," + table[0][3] + "\n")

# table is now a 2D array, lines then columns
for x in range(1,len(table)):
    # print "x = " + str(x)
    # get the 2nd col
    # print table[x][1]
    address = table[x][1]
    new_address = table[x][1]
    for m in re.finditer("^\s\d+|^\d+", address):
        # m = re.search('\d',address)
        # print "address = " + address
        # print "m.start() = " + str(m.start())
        # print "m.end() = " + str(m.end())
        # print "substring = " + address[m.start():m.end()]
        address_number = address[m.start():m.end()]
        address_label = address[m.end()+1:]
        new_address = address_label + " " + address_number

        # print "new_address = '" + new_address + "'"
        # print ""
    theLine = table[x][0] + "," + new_address + "," + table[x][2] + "," + table[x][3] + "\n"
    writeFile.write(theLine);
    # m.group(0)
