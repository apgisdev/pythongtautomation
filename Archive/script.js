console.log('begin program');
console.log('');

//=============================================================================

// import the file system of node
fs = require('fs');

// open the file and set the returned id to the variable. [ https://nodejs.org/api/fs.html#fs_fs_opensync_path_flags_mode ]
// var fileDescriptor = fs.openSync('./test.txt','w');

// [ https://nodejs.org/api/fs.html#fs_fs_readsync_fd_buffer_offset_length_position ]
// fs.readSync(fd, buffer, offset, length, position)

var readIn = fs.readFileSync('./test.txt');

var numberOfRows = 27;

var numberOfCols = 8;

var schedule = {
  numberOfRows: 27,
  numberOfCols: 8,
  tripTimes: [] //array of arrays
};

// console.log('readIn: ' + readIn);
// console.log('typeof readIn: ' + typeof readIn.toString());

var splitStr = readIn.toString().split(' ');
var splitStr = splitStr.toString().split('\n');
var splitStr = splitStr.toString().split(',');

var testArr = [];
var item;

console.log('readIn.indexOf(\'\n\'): '+readIn.toString().substring(0,readIn.indexOf('\n')));


for(var i = 0; i < numberOfRows; i++){
  for(var j = 0; j < numberOfCols; j++){

    item = splitStr[ i + (j * numberOfRows)];

    if(typeof item !== undefined && item !== "" && item !== null){
      testArr.push(item);
    } else {
      console.log('garbage data detected! i = ' + i + ', j = ' + j);
    }
  }
  schedule.tripTimes.push(testArr);
  testArr = [];
}
// console.log('schedule.tripTimes: ' + JSON.stringify(schedule.tripTimes,null,2));

// NOW Build the big n^3 loop to build out the file

var timeString = '';
var previousTimeString = '';
var indexOfColon = -1;
var startUTCString = '';
var endUTCString = '';
var minunteInteger = -1;

for(var i = 0; i < schedule.tripTimes.length; i++){

  for(var j = 0; j < schedule.tripTimes[schedule.tripTimes.length-1].length; j++){

    timeString = schedule.tripTimes[i][j];

    if(timeString !== '—'){

      previousTimeString = timeString;
        // if(previousTimeString === '—'){ previousTimeString = schedule.tripTimes[i][j-2]; }

      if(schedule.tripTimes[i].length > 0){
        indexOfColon = previousTimeString.indexOf(':');
      }

      //startUTCString
      if(indexOfColon === 1){
        if(j === 0){
          startUTCString = '';
        } else {
          // others
          // 7:30 -> 07:30:30
          // if( previousTimeString[indexOfColon+1] !== "0"){
            // minunteInteger = parseInt(previousTimeString[indexOfColon+1]);
          // } else {
            // minunteInteger = parseInt(previousTimeString[indexOfColon+2]);
          // }
          minunteInteger = parseInt(previousTimeString.substring(indexOfColon+1,indexOfColon+3));
          // console.log('****** minunteInteger: ' + minunteInteger);
          minunteInteger--;

          // console.log('****** minunteInteger: ' + minunteInteger);
          if( minunteInteger === -1){
            minunteInteger = 59;
            var hourInteger = parseInt(previousTimeString.substring(0,indexOfColon));
            hourInteger--;
            if(hourInteger === 0){ hourInteger = 12; }
          }
          if(minunteInteger < 10){
            startUTCString =  "0" + previousTimeString[0] + ":0" + minunteInteger + ":30";
          } else if (minunteInteger >= 10){
            startUTCString =  "0" + previousTimeString[0] + ":" + minunteInteger + ":30";
          }
        }

      } else if(indexOfColon === 2){
        //this is xx:xx
        if(j === 0){
          startUTCString = '';
        } else {
          // others
          // 7:30 -> 07:30:30

          minunteInteger = parseInt(timeString.substring(0,2));
          minunteInteger--;

          startUTCString = minunteInteger + ":3" +timeString[3] + ":00";
        }

      } else if(indexOfColon == -1){
        console.log('colon not found...');
      } else {
        console.log('something strange happened with the indexOfColon finding...');
      }


      // endUTCString

      if(schedule.tripTimes[i].length > 0){
        indexOfColon = timeString.indexOf(':');
      }

      if(indexOfColon === 1){
        //this is x:xx
        if(j ===  schedule.tripTimes[schedule.tripTimes.length-1].length - 1){
          // console.log('j = ' + j);
          // first
          startUTCString = endUTCString;
          endUTCString = '';
        } else {
          endUTCString = "0" + timeString + ":00";
        }
      } else if(indexOfColon === 2){
        //this is xx:xx
        if(j ===  schedule.tripTimes[schedule.tripTimes.length-1].length){
          // first
          endUTCString = '';
        } else {
          endUTCString = timeString + ":00";
        }

      } else if(indexOfColon == -1){

      } else {
        console.log('something strange happened with the indexOfColon finding...');
      }

      // console.log('indexOfColon: ' + indexOfColon);

      // console.log(i+','+startUTCString+','+endUTCString+','+',');
    }

  }
  console.log('');
  startUTCString = '';
  endUTCString = '';

}



//=============================================================================

console.log('');
console.log('end program');

// } else {
//   // others 7:30 -> 07:30:30
//
//   minunteInteger = parseInt(timeString[0]);
//   minunteInteger--;
//
//   endUTCString = "0" + minunteInteger + ":3" +timeString[3];
// }

///==============
// console.log('begin program');
// console.log('');
//
// //=============================================================================
//
// // import the file system of node
// fs = require('fs');
//
// // open the file and set the returned id to the variable. [ https://nodejs.org/api/fs.html#fs_fs_opensync_path_flags_mode ]
// // var fileDescriptor = fs.openSync('./test.txt','w');
//
// // [ https://nodejs.org/api/fs.html#fs_fs_readsync_fd_buffer_offset_length_position ]
// // fs.readSync(fd, buffer, offset, length, position)
//
// var readIn = fs.readFileSync('./test.txt');
//
// var numberOfRows = 27;
//
// var numberOfCols = 8;
//
// var schedule = {
//   numberOfRows: 12,
//   numberOfCols: 8,
//   tripTimes: [[]] //array of arrays
// };
//
// // console.log('readIn: ' + readIn);
// // console.log('typeof readIn: ' + typeof readIn.toString());
//
// var splitStr = readIn.toString().split(' ');
// var splitStr = splitStr.toString().split('\n');
// var splitStr = splitStr.toString().split(',');
// // console.log('splitStr: ' + JSON.stringify(splitStr));
//
// var testArr = [];
// var item;
//
// for(var i = 0; i < numberOfRows; i++){
//   for(var j = 0; j < numberOfCols; j++){
//
//     item = splitStr[ i + (j * numberOfRows)];
//
//     if(typeof item !== undefined && item !== "" && item !== null){
//       testArr.push(item);
//     } else {
//       console.log('garbage data detected! i = ' + i + ', j = ' + j);
//     }
//   }
//   schedule.tripTimes.push(testArr);
//   testArr = [];
// }
// console.log('schedule.tripTimes: ' + JSON.stringify(schedule.tripTimes,null,2));
//
// // NOW Build the big n^3 loop to build out the file
//
// var timeString = '';
// var previousTimeString = '';
// var indexOfColon = -1;
// var startUTCString = '';
// var endUTCString = '';
// var minunteInteger = -1;
//
// for(var i = 0; i < schedule.tripTimes.length; i++){
//
//   // console.log('123');
//
//   // console.log('schedule.tripTimes[0].length: ' + schedule.tripTimes[schedule.tripTimes.length-1].length);
//
//   for(var j = 0; j < schedule.tripTimes[schedule.tripTimes.length-1].length; j++){
//
//     timeString = schedule.tripTimes[i][j];
//
//     if(schedule.tripTimes[i][j-1]){
//       previousTimeString = schedule.tripTimes[i][j-1];
//       if(previousTimeString === '—'){ previousTimeString = schedule.tripTimes[i][j-2]; }
//     }
//
//     if(schedule.tripTimes[i].length > 0){
//       indexOfColon = previousTimeString.indexOf(':');
//     }
//
//     //startUTCString
//     if(indexOfColon === 1){
//       if(j === 0){
//         startUTCString = '';
//       } else {
//         // others
//         // 7:30 -> 07:30:30
//
//         minunteInteger = parseInt(previousTimeString[indexOfColon+1]);
//         minunteInteger--;
//         if( minunteInteger === -1){
//           minunteInteger = 59;
//           var hourInteger = parseInt(previousTimeString[0]);
//           hourInteger--;
//           if(hourInteger === 0){ hourInteger = 12; }
//         }
//         // console.log('timeString: '+timeString);
//         console.log('timeString[3]: '+timeString[3])
//         if(minunteInteger < 10){
//           startUTCString =  "0" + previousTimeString[0] + ":0" + minunteInteger + ":3" +timeString[3];
//         } else if (minunteInteger >= 10){
//           startUTCString =  "0" + previousTimeString[0] + ":" + minunteInteger + ":3" +timeString[3];
//         }
//       }
//
//     } else if(indexOfColon === 2){
//       //this is xx:xx
//       if(j === 0){
//         startUTCString = '';
//       } else {
//         // others
//         // 7:30 -> 07:30:30
//
//         minunteInteger = parseInt(timeString.substring(0,2));
//         minunteInteger--;
//
//         startUTCString = minunteInteger + ":3" +timeString[3];
//       }
//
//     } else if(indexOfColon == -1){
//       console.log('colon not found...');
//     } else {
//       console.log('something strange happened with the indexOfColon finding...');
//     }
//
//
//     // endUTCString
//
//     if(schedule.tripTimes[i].length > 0){
//       indexOfColon = timeString.indexOf(':');
//     }
//
//     if(indexOfColon === 1){
//       //this is x:xx
//       if(j ===  schedule.tripTimes[schedule.tripTimes.length-1].length){
//         // first
//         startUTCString = endUTCString;
//         endUTCString = '';
//       } else {
//         endUTCString = "0" + timeString + ":00";
//       }
//     } else if(indexOfColon === 2){
//       //this is xx:xx
//       if(j ===  schedule.tripTimes[schedule.tripTimes.length-1].length){
//         // first
//         endUTCString = '';
//       } else {
//         endUTCString = timeString + ":00";
//       }
//
//     } else if(indexOfColon == -1){
//
//     } else {
//       console.log('something strange happened with the indexOfColon finding...');
//     }
//
//     // console.log('indexOfColon: ' + indexOfColon);
//
//     console.log(i+','+startUTCString+','+endUTCString+','+',');
//
//   }
//   console.log('');
//   startUTCString = '';
//   endUTCString = '';
//
// }
//
//
//
// //=============================================================================
//
// console.log('');
// console.log('end program');
//
// // } else {
// //   // others 7:30 -> 07:30:30
// //
// //   minunteInteger = parseInt(timeString[0]);
// //   minunteInteger--;
// //
// //   endUTCString = "0" + minunteInteger + ":3" +timeString[3];
// // }
