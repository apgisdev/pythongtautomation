from tokenize import tokenize
import csv as cv

print "Hello World";

routeNumber = "_Industrial_Park"

read_OldStopsFile = open('Rt'+str(routeNumber)+'_common_stop_times.txt','rU') # stop times

# read_StopsMasterIndex = open('result_stops_master_index_csv.txt','rU')

write_NewStopTimeFile = open('result_Rt'+str(routeNumber)+'_common_out_in_stops_csv.txt','w')

# from 6:27 AM to 6:26 PM (ev30 min)
# Saturday is different

minutes_on_hour =  ["27","","","34","37","41","46","49","","56"]
minutes_off_hour = ["57","","","4","7","11","16","19","","26"]
stops_to_make = ["S506","S201","S200","S503","S504","S505","S504","S503","S199","S506"]
theLine = ""

for i in range(0,13):
    trip_id = "T" + str(i+157)

    for j in range(0,len(minutes_on_hour)):

        # on hour

        # arrival_time
        # print "len(minutes_on_hour[ "+str(j)+" ]) " + str(len(minutes_on_hour[ j ]))
        arrival_time_hour = ""
        arrival_time_min = ""
        arrival_time_sec = "30"

        # hour
        arrival_time_hour = str(i+6)

        if( len(arrival_time_hour) is 1 ):
            arrival_time_hour = "0" + arrival_time_hour

        # min

        if( len(minutes_on_hour[ j ]) > 0 ):
            arrival_time_min = str(int(minutes_on_hour[ j ]) - 1)

        if ( len(arrival_time_min) is 1 ):
            arrival_time_min = "0" + arrival_time_min

        # if there at all
        if ( len(arrival_time_min) > 0 ):
            arrival_time = arrival_time_hour + ":" + arrival_time_min + ":" + arrival_time_sec
        else:
            arrival_time = ""
        print "arrival_time = '" + arrival_time + "'"

        # departure_time

        depart_time_hour = ""
        depart_time_min = minutes_on_hour[ j ]
        depart_time_sec = "00"

        # hour
        depart_time_hour = str(i+6)

        if( len(depart_time_hour) is 1 ):
            depart_time_hour = "0" + depart_time_hour

        # min
        if ( len(depart_time_min) is 1 ):
            depart_time_min = "0" + depart_time_min

        # if there at all
        if ( len(depart_time_min) > 0 ):
            depart_time = depart_time_hour + ":" + depart_time_min + ":" + depart_time_sec
        else:
            depart_time = ""
        print "depart_time = '" + depart_time + "'"

        stop_id = stops_to_make[j]
        stop_sequence = str(j)

        # if start or finish the arrival time is the same as the depar time
        if( j is 0 or j is (len(minutes_on_hour) - 1)):
            arrival_time = depart_time

        theLine = trip_id + "," + arrival_time + "," + depart_time + "," + stop_id + "," + stop_sequence
        write_NewStopTimeFile.write(theLine + "\n")
    # print theLine

# off hour
for i in range(0,13):
    trip_id = "T" + str(i+170)

    for j in range(0,len(minutes_off_hour)):
        # arrival_time

        arrival_time_hour = ""
        arrival_time_min = ""
        arrival_time_sec = "30"

        # hour
        if ( j is 0 ):
            arrival_time_hour = str(i+6)
        else:
            arrival_time_hour = str(i+7)

        if( len(arrival_time_hour) is 1 ):
            arrival_time_hour = "0" + arrival_time_hour

        # min

        if( len(minutes_off_hour[ j ]) > 0 ):
            arrival_time_min = str(int(minutes_off_hour[ j ]) - 1)

        if ( len(arrival_time_min) is 1 ):
            arrival_time_min = "0" + arrival_time_min

        # if there at all
        if ( len(arrival_time_min) > 0 ):
            arrival_time = arrival_time_hour + ":" + arrival_time_min + ":" + arrival_time_sec
        else:
            arrival_time = ""
        print "arrival_time = '" + arrival_time + "'"

        # departure_time

        depart_time_hour = ""
        depart_time_min = minutes_off_hour[ j ]
        depart_time_sec = "00"

        # hour
        if ( j is 0 ):
            depart_time_hour = str(i+6)
        else:
            depart_time_hour = str(i+7)

        if( len(depart_time_hour) is 1 ):
            depart_time_hour = "0" + depart_time_hour

        # min
        if ( len(depart_time_min) is 1 ):
            depart_time_min = "0" + depart_time_min

        # if there at all
        if ( len(depart_time_min) > 0 ):
            depart_time = depart_time_hour + ":" + depart_time_min + ":" + depart_time_sec
        else:
            depart_time = ""
        print "depart_time = '" + depart_time + "'"

        stop_id = stops_to_make[j]
        stop_sequence = str(j)

        # if start or finish the arrival time is the same as the depar time
        if( j is 0 or j is (len(minutes_on_hour) - 1)):
            arrival_time = depart_time

        theLine = trip_id + "," + arrival_time + "," + depart_time + "," + stop_id + "," + stop_sequence
        write_NewStopTimeFile.write(theLine + "\n")
    # print theLine


def buildIndustrialPark():
    
    routeNumber = "_Industrial_Park"

    read_OldStopsFile = open('Rt'+str(routeNumber)+'_common_stop_times.txt','rU') # stop times

    # read_StopsMasterIndex = open('result_stops_master_index_csv.txt','rU')

    write_NewStopTimeFile = open('result_Rt'+str(routeNumber)+'_common_out_in_stops_csv.txt','w')

    # from 6:27 AM to 6:26 PM (ev30 min)
    # Saturday is different

    minutes_on_hour =  ["27","","","34","37","41","46","49","","56"]
    minutes_off_hour = ["57","","","4","7","11","16","19","","26"]
    stops_to_make = ["S506","S201","S200","S503","S504","S505","S504","S503","S199","S506"]
    theLine = ""

    for i in range(0,13):
        trip_id = "T" + str(i+157)

        for j in range(0,len(minutes_on_hour)):

            # on hour

            # arrival_time
            # print "len(minutes_on_hour[ "+str(j)+" ]) " + str(len(minutes_on_hour[ j ]))
            arrival_time_hour = ""
            arrival_time_min = ""
            arrival_time_sec = "30"

            # hour
            arrival_time_hour = str(i+6)

            if( len(arrival_time_hour) is 1 ):
                arrival_time_hour = "0" + arrival_time_hour

            # min

            if( len(minutes_on_hour[ j ]) > 0 ):
                arrival_time_min = str(int(minutes_on_hour[ j ]) - 1)

            if ( len(arrival_time_min) is 1 ):
                arrival_time_min = "0" + arrival_time_min

            # if there at all
            if ( len(arrival_time_min) > 0 ):
                arrival_time = arrival_time_hour + ":" + arrival_time_min + ":" + arrival_time_sec
            else:
                arrival_time = ""
            print "arrival_time = '" + arrival_time + "'"

            # departure_time

            depart_time_hour = ""
            depart_time_min = minutes_on_hour[ j ]
            depart_time_sec = "00"

            # hour
            depart_time_hour = str(i+6)

            if( len(depart_time_hour) is 1 ):
                depart_time_hour = "0" + depart_time_hour

            # min
            if ( len(depart_time_min) is 1 ):
                depart_time_min = "0" + depart_time_min

            # if there at all
            if ( len(depart_time_min) > 0 ):
                depart_time = depart_time_hour + ":" + depart_time_min + ":" + depart_time_sec
            else:
                depart_time = ""
            print "depart_time = '" + depart_time + "'"

            stop_id = stops_to_make[j]
            stop_sequence = str(j)

            # if start or finish the arrival time is the same as the depar time
            if( j is 0 or j is (len(minutes_on_hour) - 1)):
                arrival_time = depart_time

            theLine = trip_id + "," + arrival_time + "," + depart_time + "," + stop_id + "," + stop_sequence
            write_NewStopTimeFile.write(theLine + "\n")
        # print theLine

    # off hour
    for i in range(0,13):
        trip_id = "T" + str(i+170)

        for j in range(0,len(minutes_off_hour)):
            # arrival_time

            arrival_time_hour = ""
            arrival_time_min = ""
            arrival_time_sec = "30"

            # hour
            if ( j is 0 ):
                arrival_time_hour = str(i+6)
            else:
                arrival_time_hour = str(i+7)

            if( len(arrival_time_hour) is 1 ):
                arrival_time_hour = "0" + arrival_time_hour

            # min

            if( len(minutes_off_hour[ j ]) > 0 ):
                arrival_time_min = str(int(minutes_off_hour[ j ]) - 1)

            if ( len(arrival_time_min) is 1 ):
                arrival_time_min = "0" + arrival_time_min

            # if there at all
            if ( len(arrival_time_min) > 0 ):
                arrival_time = arrival_time_hour + ":" + arrival_time_min + ":" + arrival_time_sec
            else:
                arrival_time = ""
            print "arrival_time = '" + arrival_time + "'"

            # departure_time

            depart_time_hour = ""
            depart_time_min = minutes_off_hour[ j ]
            depart_time_sec = "00"

            # hour
            if ( j is 0 ):
                depart_time_hour = str(i+6)
            else:
                depart_time_hour = str(i+7)

            if( len(depart_time_hour) is 1 ):
                depart_time_hour = "0" + depart_time_hour

            # min
            if ( len(depart_time_min) is 1 ):
                depart_time_min = "0" + depart_time_min

            # if there at all
            if ( len(depart_time_min) > 0 ):
                depart_time = depart_time_hour + ":" + depart_time_min + ":" + depart_time_sec
            else:
                depart_time = ""
            print "depart_time = '" + depart_time + "'"

            stop_id = stops_to_make[j]
            stop_sequence = str(j)

            # if start or finish the arrival time is the same as the depar time
            if( j is 0 or j is (len(minutes_on_hour) - 1)):
                arrival_time = depart_time

            theLine = trip_id + "," + arrival_time + "," + depart_time + "," + stop_id + "," + stop_sequence
            write_NewStopTimeFile.write(theLine + "\n")
        # print theLine
