#!/usr/bin/env python2.7
# ====================================================================================================
# Title: buildStopTimes.py
# Author: Harrison Welch
# Desc: Build the stop_times.txt file of google transit
#   Also does the stops.txt file
#   NOTE: this does not touch shapefile
# ====================================================================================================

# ====================================================================================================
# IMPORTS
# ====================================================================================================

import sys
import os
import traceback
sys.path.insert(0, './Route_Industrial_Park')
sys.path.insert(0, './MasterIndexToStops')
from tokenize import tokenize
import csv as cv
import zipfile as zf
from optparse import OptionParser
from datetime import date,datetime
import zlib
import industrialParkFunction as ipf #from RouteIndustrial_Park
from master_stops_to_gfts_stops import index_stops_to_google_stops
from csv_stops_to_master_index_stops import arcmap_stops_to_index_stops

# ====================================================================================================
# CLASSES
# ====================================================================================================

class Tranis_Feed:
    def __init__(self, agency=None, calendar_dates=[], calendars=[], routes=[], shapes=[], stop_times="", stops="", trips=""):
        self.agency = agency
        self.calendar_dates = calendar_dates
        self.calendars = calendars
        self.routes = routes
        self.shapes = shapes
        self.stop_times = stop_times
        self.stops = stops
        self.trips = trips

    def set_agency(self, agency):
        self.agency = agency

    def add_calendar_date(self, calendar_date):
        self.calendar_date.append(calendar_date)

    def add_calendar(self, calendar):
        self.calendars.append(calendar)

    def add_calendar(self, route):
        self.routes.append(route)

    def add_shape(self, shape):
        self.shapes.append(shape)

    def build_agency_file(self):
        holding_file_name = "ActiveGoogleTransitSubmission"
        agency_file = open(holding_file_name + "/" + "agency.txt", "w+")
        agency_file.write(str(self.get_agency_name()) + "," +
                          str(self.get_agency_url()) + "," +
                          str(self.get_agency_timezone) + "," +
                          str(self.get_agency_lang) + "\n")

class Agency:
    def __init__(self, agency_name, agency_url, agency_timezone, agency_lang):
        self.agency_name = agency_name
        self.agency_url = agency_url
        self.agency_timezone = agency_timezone
        self.agency_lang = agency_lang

    def get_agency_name(self):
        return self.agency_name

    def get_agency_url(self):
        return self.agency_url

    def get_agency_timezone(self):
        return self.agency_timezone

    def get_agency_lang(self):
        return self.agency_lang

class Calendar_Date:
    def __init__(self, service_id, date, exception_type):
        self.service_id = service_id
        self.date = date
        self.exception_type = exception_type

class Calendar:
    def __init__(self, service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, start_date, end_date):
        self.service_id = service_id
        self.monday = monday
        self.tuesday = tuesday
        self.wednesday = wednesday
        self.thursday = thursday
        self.friday = friday
        self.saturday = saturday
        self.start_date = start_date
        self.end_date = end_date

class Route:

    def __init__(self, route_name, route_start_index, route_stop_index):
        self.route_name = route_name
        self.route_start_index = route_start_index
        self.route_stop_index = route_stop_index

    def get_route_name(self):
        return self.route_name

    def get_route_start_index(self):
        return self.route_start_index

    def get_route_stop_index(self):
        return self.route_stop_index

# ====================================================================================================
# FUNCTIONS
# ====================================================================================================

# This function zips all the ActiveGoogleTransitSubmission folder into a compressed zip folder
def zip_up():

    from datetime import date,datetime

    print "Zipping up the 'ActiveGoogleTransitSubmission' directory"

    # get today's date (ddmmyyyy)
    date = str(date.today().strftime("%d_%m_%Y"))

    # get today's clock time (hh:mm:[A|P]M)
    time = str(datetime.now().strftime("%I_%M_%p"))

    # directory for storing zip files
    directory_name = "zip_files"

    # if the directory does not exist, make one
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)

    # build the file name in the zip file directory
    filename = str(directory_name) + "/" + "google_transit_"+date+"_at_"+time+".zip"

    # compile the zipFile
    # ZIP_DEFLATED means to compress the file down
    with zf.ZipFile(filename,'w',zf.ZIP_DEFLATED) as myzip:
        myzip.write('ActiveGoogleTransitSubmission/agency.txt','agency.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/calendar_dates.txt','calendar_dates.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/calendar.txt','calendar.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/routes.txt','routes.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/shapes.txt','shapes.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/stop_times.txt','stop_times.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/stops.txt','stops.txt',zf.ZIP_DEFLATED)
        myzip.write('ActiveGoogleTransitSubmission/trips.txt','trips.txt',zf.ZIP_DEFLATED)
        if options.flag_verbose:
            myzip.printdir()
        myzip.close()

# build the route for the selected route number from the start position route_trip_id_start
def build_route(route_tuple):

    routeNumber = route_tuple.get_route_name()

    # ====================================================================================================
    # FILE OPEN
    # ====================================================================================================

    # grab the times
    route_stop_times = open('Route'+str(routeNumber)+'/Rt'+str(routeNumber)+'_common_stop_times.txt','rU') # stop times

    # grab the master index
    arcmap_csv_file = open('MasterIndexToStops/result_stops_master_index_csv.txt','rU')

    # grab the route need for the bus to travel as a list of stops (with GPS coords)
    route_stops = open('Route'+str(routeNumber)+'/Rt'+str(routeNumber)+'_common_out_in_stops_csv.txt','rU')

    # open the
    result_route_stops = open('Route'+str(routeNumber)+'/result_Rt'+str(routeNumber)+'_common_out_in_stops_csv.txt','w')

    # ====================================================================================================
    # CSV PROCESSING
    # ====================================================================================================

    # break down the list of stops that the bus runs past into a 2D array
    route_stops_list = []
    csvFile = cv.reader(route_stops,delimiter=',', quotechar='\n')
    for row in csvFile:
        route_stops_list.append(row)

    # break down master index into a 2D array
    arcmap_csv_file_list = []
    masterCsvFile = cv.reader(arcmap_csv_file,delimiter=',', quotechar='\n')
    for row in masterCsvFile:
        arcmap_csv_file_list.append(row)

    # break the times that the bus makes a scheduled stops into a 2D array
    route_stop_times_list = []
    timesCsvFile = cv.reader(route_stop_times,delimiter=',', quotechar='\n')
    for row in timesCsvFile:
        route_stop_times_list.append(row)

    # array that hols the stops where the bus makes a scheduled (timed) stop
    routed_stops = []

    # ====================================================================================================
    # Begin matching the "routed" (sequenced stop list) to the master list
    # 1. Grab the lat and lon of the sequenced "routed" list
    # 2. Grab the lat and lon of the master index list
    # 3. If both the lats and lons match, we have a match
    # ====================================================================================================

    epsilon = 1.0e-6

    if options.flag_verbose:
        print "epsilon: " + str(epsilon)
        print "route_stops_list = ", route_stops_list
        print "len(route_stops_list) = ", len(route_stops_list)
    # for each
    for x in range(0,len(route_stops_list)):
        # grab the lattitude and longitude of the routed stop
        lon = route_stops_list[x][len(route_stops_list[x])-2]
        lat = route_stops_list[x][len(route_stops_list[x])-3]

        # Routes 1 and 1_FL are from a different data set
        # we need to change what columns we grab from
        if routeNumber == "1" or routeNumber == "1_FL":
            lon = route_stops_list[x][len(route_stops_list[x])-2]
            lat = route_stops_list[x][len(route_stops_list[x])-1]

        if routeNumber == "_Industrial_Park":
            lon = route_stops_list[x][len(route_stops_list[x])-3]
            lat = route_stops_list[x][len(route_stops_list[x])-4]

        if options.flag_verbose:
            print "arcmap_csv_file_list = ", arcmap_csv_file_list
            print "len(arcmap_csv_file_list) = ", len(arcmap_csv_file_list)
        for y in range(0,len(arcmap_csv_file_list)):

            # grab the lat and lon out of the arcmap list
            masterLon = arcmap_csv_file_list[y][3]
            masterLat = arcmap_csv_file_list[y][2]

            if routeNumber == "_Industrial_Park":
                # swap positions
                masterLon = arcmap_csv_file_list[y][2]
                masterLat = arcmap_csv_file_list[y][3]

            if options.flag_verbose:
                print "lon = ", lon, ", masterLon = ", masterLon
                print "lat = ", lat, ", masterLat = ", masterLat
                if ( (abs(float(lon) - float(masterLon)) < epsilon ) and (abs(float(lat) - float(masterLat)) < epsilon) ) or ((float(lon) - float(masterLon)) == 0.0 ) and ((float(lat) - float(masterLat)) == 0.0):
                    print "found match of stops!"

            # compare the route lon,lat to each of the arcmap_csv_file_list's lon,lat
            # if the the stop the buses passes a stop in the master index, at it to the routed stop
            # find a match
            if ( (abs(float(lon) - float(masterLon)) < epsilon ) and (abs(float(lat) - float(masterLat)) < epsilon) ) or ((float(lon) - float(masterLon)) == 0.0 ) and ((float(lat) - float(masterLat)) == 0.0):
                routed_stops.append(arcmap_csv_file_list[y])

    row_format = "{:>15}" * len(routed_stops[0])

    # now I know the stops on the route provided, now to just make the list of stop_times.txt
    for x in range(0, len(routed_stops)):
        if (routed_stops[x][4] == '1'):
            print "| {0:<15} {1:<80} {2:<15} {3:<15} {4:<10} |".format(routed_stops[x][0], routed_stops[x][1], routed_stops[x][2], routed_stops[x][3], routed_stops[x][4])

    print "==============================================================================================================================================="
    print ""

    # Now begin grabing stop times (or blank if not stopping but passes by that stop)

    timedStopIndex = 0 # 22
    trip_id_start = route_tuple.get_route_start_index()
    trip_id_end = route_tuple.get_route_stop_index()

    # print the details of the current
    if options.flag_verbose:
        print "routeNumber : " + str(routeNumber)
        print "trip_id_start : " + str(trip_id_start)
        print "trip_id_end : " + str(trip_id_end)

    for z in range( trip_id_start, trip_id_end ):

        for x in range(0, len(routed_stops)):
            # get the unique (to the entire stop_time.txt file) trip id based
            # on the
            trip_id = "T" + str(z)
            arrival_time = ""
            departure_time = ""

            # if there is a '1' in the fifth (5th) position,
            # this indicates the stop reuires a time
            if (routed_stops[x][4] == '1'):
                # grab the arrival time out of the 2nd column
                arrival_time = route_stop_times_list[timedStopIndex][1]
                # grab the departure_time out of the 3rd column
                departure_time = route_stop_times_list[timedStopIndex][2]
                # increment the index of what line of stop times we can grab
                timedStopIndex = timedStopIndex + 1

            # grab the stop_id for the start of theLine
            stop_id = routed_stops[x][0]

            # each line of the trip (not the whole file) needs a unique id
            stop_sequence = str(x)

            # combine the line together
            theLine = trip_id + "," + arrival_time + "," + departure_time + "," + stop_id + "," + stop_sequence + "\n"

            # write to the
            result_route_stops.write(theLine)
    return 0

# Store all the results of the routed stops into the stop_times.txt file
def store_in_file( route_tuples ):
    holding_file_name = "ActiveGoogleTransitSubmission"
    # open the stop_times.txt file
    writeFile = open( holding_file_name + "/stop_times.txt",'w')
    # write the top title bar which labels the
    writeFile.write("trip_id,arrival_time,departure_time,stop_id,stop_sequence\n")
    for x in range(0,len(route_tuples)):

        # if verbose mode, print
        if options.flag_verbose:
            print "available_routes[x] : " + str(available_routes[x])

        route_name = route_tuples[x].get_route_name()
        # print "route_name = " + str(route_name)
        readFile = open('Route'+str( route_name )+'/result_Rt'+str( route_name )+'_common_out_in_stops_csv.txt','r')

        # print readFile[0]
        for line in readFile:
            writeFile.write(line)

def remove_unused_stops():
    # open the stop_times file to read unique stop ids
    stop_times_file = open("ActiveGoogleTransitSubmission/stop_times.txt","rU")
    # open the stops.txt file to read in what stops we can use
    stops_file = open("ActiveGoogleTransitSubmission/stops.txt","rU")
    # open or create the unused stop text file to be filled with stops not used by the transit feed
    # NOTE: this can be omitted, it's mostly for safety
    unused_stops_in_transit_feed_file = open("unused_stops_in_transit_feed.txt", "w+")

    # parse the stop times file into a 2D array
    stop_times_list = []
    stop_times_list_csv = cv.reader(stop_times_file,delimiter=',', quotechar='\n')
    for row in stop_times_list_csv:
        stop_times_list.append(row)


    # parse the stops file into a 2D array
    stops_list = []
    stops_list_csv = cv.reader(stops_file,delimiter=',', quotechar='\n')
    for row in stops_list_csv:
        stops_list.append(row)

    # ==========================================================================

    # create a set of stops_list
    # NOTE: this is always a unique set, adding an already existing element will
    # yeild the same array
    stop_times_set = set([])

    # set the position of the stop_id as the 4th column
    stop_id_position_in_line = 3

    # for each line in the stop_times_list, add a unique stop id to the stop_times_set
    for i in range(0,len(stop_times_list)):

        # get stop_id
        stop_id = stop_times_list[ i ][ stop_id_position_in_line ]

        # add a unique to set
        stop_times_set.add( stop_id )

    # if verbose mode, tell me the unique stop ids
    if options.flag_verbose:
        print "stop_times_set = ", stop_times_set

    # the stop id is in the 1st column
    stop_id_position_in_line = 0

    # for each row in the stops.txt
    for i in range(0,len(stops_list)):

        # get the stop id out of stops_list
        stop_id = stops_list[ i ][ stop_id_position_in_line ]

        # if the stop is not used tombstone the line
        if len(stop_times_set.intersection(set([stop_id]))) == 0:
            # write the line to the file
            for j in range(0, len(stops_list[i])):
                # write the unused stop to the unused stop file
                unused_stops_in_transit_feed_file.write(str(stops_list[i][j]))
                # if not the last column in the array write a comma
                if j != (len(stops_list[i]) - 1):
                    unused_stops_in_transit_feed_file.write(",")
            # write newline
            unused_stops_in_transit_feed_file.write("\n")

            # tombstone the line for later disposal
            stops_list[i] = "TOMBSTONE"

    # build a new list of stops
    new_stops_list = []

    # for each item in the stops_list:
    for i in range (0, len(stops_list)):
        # if the array index is not a tombstone
        if stops_list[i] != "TOMBSTONE":
            # append to the new list of stops
            new_stops_list.append(stops_list[i])

    # if in verbose mode, print the new list
    if options.flag_verbose:
        print "new_stops_list = ", new_stops_list

    # close the file to stop "reading"
    stops_file.close()

    # open stops.txt again in "write-create" mode
    stops_file = open("ActiveGoogleTransitSubmission/stops.txt","w+")

    # write out to the file
    # for each row in new_stops_list
    for i in range (0, len(new_stops_list)):
        # for each col in new_stops_list
        for j in range (0, len(new_stops_list[i])):
            # write out to the file
            stops_file.write(new_stops_list[i][j])
            # if the not the last column write a comma to seperate the cols
            if j != (len(new_stops_list[i]) - 1):
                stops_file.write(",")
        # write a new line character
        stops_file.write("\n")

    return

# set command line arguments of this program
# return the options and args as a tuple
def set_parser_options():
    parser = OptionParser()
    parser.add_option("-r", "--route",
                    action="store", type="string", dest="route_number")
    parser.add_option("-z", "--zipup",
                    action="store_true", dest="flag_zipup")
    parser.add_option("-v", "--verbose",
                    action="store_true", dest="flag_verbose")
    parser.add_option("-a", "--all",
                    action="store_true", dest="flag_all") # do all routes in available_routes
    return parser.parse_args()

def print_available_routes(route_tuples):
    # print available routes
    available_routes_string = "Available routes: "
    for i in range(0,len(route_tuples)):
        route_name = route_tuples[i].get_route_name()
        available_routes_string = available_routes_string + route_name
        if i != (len(route_tuples) - 1):
            available_routes_string = available_routes_string + ", "
    print available_routes_string

def get_tab_string(string):
    length_string = len(string)
    tab_string = ""
    number_of_characters_per_tab = 8
    for i in range(0, (length_string / (number_of_characters_per_tab) + 1 )):
        tab_string = tab_string + "\t"
    return tab_string

def build_trips_file():
    holding_file_name = "ActiveGoogleTransitSubmission"
    # 1. I need the stop_times.txt file
    stop_times_file = open( holding_file_name + "/" + "stop_times.txt", "r")
    # 2. I need the trips.txt file to read from
    trips_file = open( holding_file_name + "/" + "trips.txt", "r")
    # 3. Read from the stops.txt file for the "T___" vales

    # break down the list of stops that the bus runs past into a 2D array
    stop_times_file_list = []
    stop_times_file_cv = cv.reader(stop_times_file, delimiter=',', quotechar='\n')
    for row in stop_times_file_cv:
        stop_times_file_list.append(row)

    # break up the trips file
    trips_file_list = []
    trips_file_cv = cv.reader(trips_file, delimiter=',', quotechar='\n')
    for row in trips_file_cv:
        trips_file_list.append(row)

    # print "stops_file_list = " + str(stop_times_file_list)

    # get me the set of T values from the first column
    T_values_set = set([])

    # add all unique elements
    for i in range(1, len(stop_times_file_list)):
        T_values_set.add(int(stop_times_file_list[i][0][1:]))

    # sort the list by nimber
    T_values_set = sorted(T_values_set)
    T_values_set_list = list(T_values_set)

    # kick out empty arrays for modification
    trips_file_list_non_empty = []
    for i in range(0, len(trips_file_list)):
        if trips_file_list[i] != []:
            trips_file_list_non_empty.append(trips_file_list[i])

    # 4. Adjust the 3rd column for each T value
    for i in range(0, len(trips_file_list_non_empty)-1):
        # print "^^^ trips_file_list_non_empty["+str(i)+"+1][2] =  " + str(trips_file_list_non_empty[i+1][2])
        # print "T_values_set_list["+str(i)+"] =               " + str("T" + str(T_values_set_list[i]))
        trips_file_list_non_empty[i+1][2] = "T" + str(T_values_set_list[i])
        # print "--- trips_file_list_non_empty["+str(i)+"+1][2] =  " + str(trips_file_list_non_empty[i+1][2])

    # place the modified non-empty back into the trips_file_list
    index = 0
    for i in range(0, len(trips_file_list)):
        # print "T_values_set_list["+str(i)+"] = " + str(trips_file_list[i])
        if trips_file_list[i] != []:
            trips_file_list[i] = trips_file_list_non_empty[index]
            index = index + 1

    # close the file to stop reading
    trips_file.close()

    # open the file to start writing
    trips_file = open( holding_file_name + "/" + "trips.txt", "w+")

    # write out to the file
    for i in range(0, len(trips_file_list)):
        the_line = ""
        for j in range(0, len(trips_file_list[i])):
            the_line = the_line + trips_file_list[i][j]
            if j != (len(trips_file_list[i]) - 1):
                the_line = the_line + ","
        trips_file.write(the_line + "\n")

    return

# ====================================================================================================
# MAIN
# ====================================================================================================

if __name__ == "__main__":

    # Notify Start Program
    print "Start Program: " + os.path.basename(__file__) + "\n";

    # colored text
    CGREEN = "\033[92m"
    CRED = "\033[91m"
    CEND = "\033[0m"
    BOLD = "\033[1m"
    BOLD_BLUE = "\033[1;34;40m"

    exit_code = 0

    # Setup the command line arguments
    options, args = set_parser_options()

    # convert the Arcmap-made csv file to an "index" file
    # The index file is used to build the the stop_times.txt
    arcmap_stops_to_index_stops()

    # convert the index stop file to Google Transit's stops.txt
    index_stops_to_google_stops()

    # Route Number Beging operated on
    route_tuples = [
        Route("1_FL",                0,      3),
        Route( "1",                  3,      17),
        Route( "2",                  17,     30),
        Route( "3",                  30,     57),
        Route( "4",                  57,     72),
        Route( "5_520_and_620",      72,     74),
        Route( "5",                  74,     88),
        Route( "5_last_run",         88,     89),
        Route( "6",                  89,     112),
        Route( "7",                  112,    140),
        Route( "8",                  140,    154),
        Route( "8_7pm",              154,    155),
        Route( "8_8pm",              155,    156),
        Route( "_Industrial_Park",   156,    182),
    ]

    if options.flag_all:
        # tell the user good to go and what the program is going to do
        print "[   "+CGREEN+"OK"+CEND+"   ] Begin building all routes"
        print_available_routes(route_tuples)
        print ""

        # build all available routes
        for x in range(0,len(route_tuples)):
            print "{:^50}".format(BOLD_BLUE+"Route " + str(route_tuples[x].get_route_name()) + CEND)
            print "-----------------------------------------------------------------------------------------------------------------------------------------------"
            print "|"+BOLD+" {0:<15} {1:<80} {2:<15} {3:<15} {4:<10} ".format("Stop ID", "Stop Name", "Stop Longitude", "Stop Latitude", "Timed Stop") + CEND + "|"
            print "-----------------------------------------------------------------------------------------------------------------------------------------------"
            route_tuple = route_tuples[x]
            exit_code = exit_code + build_route( route_tuple )

    elif options.route_number:
        # if the argument is present in the command line
        routeNumber = str(options.route_number)

        # test for exist in the available_routes
        try:
            # find the correct route for the argument passed in
            route_tuple = None
            for i in range(0,len(route_tuples)):
                if route_tuples[i].get_route_name() == routeNumber:
                    route_tuple = route_tuples[i]
                    break

            if route_tuple == None:
                raise

            print "[   "+CGREEN+"OK"+CEND+"   ]" + " Selecled route number " + str(options.route_number)
            print ""
            print "{:^50}".format(BOLD_BLUE+"Route " + routeNumber + CEND)
            print "-----------------------------------------------------------------------------------------------------------------------------------------------"
            print "|"+BOLD+" {0:<15} {1:<80} {2:<15} {3:<15} {4:<10} ".format("Stop ID", "Stop Name", "Stop Longitude", "Stop Latitude", "Timed Stop") + CEND + "|"
            print "-----------------------------------------------------------------------------------------------------------------------------------------------"

            # build the route from the name
            exit_code = exit_code + build_route( route_tuple )

        except:
            print "[  "+CRED+"ERROR"+CEND+"  ] This route is not available. Here are the available routes:"
            exit_code = 2
            print_available_routes(route_tuples)
            traceback.print_exc()

    else:
        print "[  "+CRED+"ERROR"+CEND+"  ] No Route specified. Use '-r' to build a specific route or '-a' to build all routes."

    # dig up the stops.txt file and remove and stops unused in stop_times.txt
    # remove any stops unseen in the file and delete those from the stops.txt
    # place the removed stops "unused_stops_in_transit_feed.txt"
    remove_unused_stops()

    # store all the result files into the ActiveGoogleTransitSubmission file
    # as stop_times.txt
    store_in_file(route_tuples)

    # zip up if parse arg says so
    if options.flag_zipup:
        # deflate the file into a timestamped zip file
        zip_up()

    build_trips_file()

    # Notify End Program
    print  {
        0:"[   "+CGREEN+"OK"+CEND+"   ]" + " The program exitted as normal.",
        1:"[   "+CRED+"ERROR"+CEND+"   ]" + " There was a small issue with building all routes."
        }.get(int(exit_code), "[  "+CRED+"ERROR"+CEND+"  ]") + " Exit code " + str(exit_code) + "."

    print "\nEND Program: " + os.path.basename(__file__) + "\n";
# ====================================================================================================
# END MAIN
# ====================================================================================================
