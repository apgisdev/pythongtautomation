# write out to a file the times to the Route 8 of CTS

# T141,05:00:00,05:00:00,S1,0
# T141,05:04:30,05:05:00,S29,1
# T141,,,S29,1
# T141,05:21:30,05:13:00,S31,3
# T141,05:31:30,05:22:00,S32,4
# T141,05:39:30,05:32:00,S33,5
# T141,05:52:00,05:57:00,S5,6
# T141,06:16:30,06:17:00,S32,8
# T141,06:28:30,06:29:00,S31,9
# T141,,,S31,9
# T141,06:39:30,06:40:00,S30,10
# T141,06:42:30,06:43:00,S29,11
# T141,06:50:00,06:50:00,S1,12

writeFile = open('route_8_times.txt','w');

# arrival minutes
arr_minutes = ["00","04","","12","21","31","52","16","28","","39","42","50"]
# departure minutes
dep_minutes = ["00","05","","13","22","32","57","17","29","","40","43","50"]

arr_minutes_midpoint = 7

# trips loop
for x in range(141,155):
    trip_id = "T" + str(x)
    # lines loop
    for y in range(0,len(arr_minutes)):
        print "y = " + str(y)
        # front 7 lines

        i = x - 141

        # ARRIVAL

        # hour
        if len(arr_minutes[y]) > 0:
            if i > 4:
                if y < arr_minutes_midpoint:
                    arrival_hour = str(i + 5)
                else:
                    arrival_hour = str(i + 6)
            else:
                if y < arr_minutes_midpoint:
                    arrival_hour = "0" + str(i + 5)
                else:
                    arrival_hour = "0" + str(i + 6)

            print "arr_minutes[y] = " + arr_minutes[y]

            # minute
            arrival_min = arr_minutes[y]

            arrival_time = arrival_hour + ":" + arrival_min + ":30"
        else:
            arrival_time = ""


        # DEPARTURE

        if len(dep_minutes[y]) > 0:
            # hour
            if i > 3:
                if y < arr_minutes_midpoint:
                    depart_hour = str(i + 5)
                else:
                    depart_hour = str(i + 6)
            else:
                if y < arr_minutes_midpoint:
                    depart_hour = "0" + str(i + 5)
                else:
                    depart_hour = "0" + str(i + 6)

            # minute
            depart_min = dep_minutes[y]

            depart_time = depart_hour + ":" + depart_min + ":00"
        else:
            depart_time = ","


        theLine = trip_id + "," + arrival_time + "," + depart_time + ",S1,0\n"
        writeFile.write(theLine)
    # writeFile.write("\n");


        # y = y + 1
        # back 6 lines
        # for b in range(6,len(dep_minutes)):
        #
        #     # ARRIVAL
        #
        #     # hour
        #     if y > 9:
        #         arrival_hour = str(y)
        #     else:
        #         arrival_hour = "0" + str(y)
        #
        #     # minute
        #     if int(dep_minutes[a]) > 9:
        #         arrival_min = dep_minutes[a]
        #     else:
        #         arrival_min = "0" + dep_minutes[a]
        #
        #     arrival_time = arrival_hour + ":" + arrival_min + ":30"
        #
        #     # DEPARTURE
        #
        #     # hour
        #     if y > 9:
        #         depart_hour = str(y)
        #     else:
        #         depart_hour = "0" + str(y)
        #
        #     # minute
        #     if int(dep_minutes[a]) > 9:
        #         depart_min = dep_minutes[a]
        #     else:
        #         depart_min = "0" + dep_minutes[a]
        #
        #     depart_time = depart_hour + ":" + depart_min + ":00"
        #
        #     theLine = trip_id + "," + arrival_time + "," + depart_time + "\n"
        #     writeFile.write(theLine)
