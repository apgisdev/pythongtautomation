#!/usr/bin/env python2.7
# ====================================================================================================
# Title: reverseList.py
# Author: Harrison Welch
# Desc: reverse a list in the the given file
# ====================================================================================================

# ====================================================================================================
# IMPORTS
# ====================================================================================================

from tokenize import tokenize
import csv as cv
import os

# ====================================================================================================
# MAIN
# ====================================================================================================

if __name__ == "__main__":
    print "Begin\t" + os.path.basename(__file__)

    CGREEN = "\033[92m"
    CRED = "\033[91m"
    CEND = "\033[0m"

    # open files
    read_file = open('candidate_to_be_reversed.txt','r');
    write_file = open('result_candidate_to_be_reversed.txt','w');

    read_file_list = list(read_file)
    # read_file_list = list(read_file)
    print "read_file_list = " + str(read_file_list)

    word = ""
    new_read_file_list = []
    for i in range(0,len(read_file_list[0])):
        if read_file_list[0][i] == " " or read_file_list[0][i] == "\n":
            new_read_file_list.append(word)
            word = ""
            continue
        word = word + str(read_file_list[0][i])

    print "new_read_file_list = " + str(new_read_file_list)

    # simple reverse list function
    reversed_new_read_file_list = list(reversed(new_read_file_list))

    for i in range(0,len(reversed_new_read_file_list)):
        write_file.write(reversed_new_read_file_list[i])
        write_file.write(" ")

    print "reversed(new_read_file_list) = " + str(list(reversed(new_read_file_list)))

    if new_read_file_list[0] == reversed_new_read_file_list[len(reversed_new_read_file_list)-1]:
        print "[   "+CGREEN+"OK"+CEND+"   ] first went to last"
    else:
        print "[  "+CRED+"FAIL"+CEND+"  ] first did does NOT equal last"
        print "new_read_file_list[0] = \t\t\t\t\t" + str(new_read_file_list[0])
        print "reversed_new_read_file_list[len(reversed_new_read_file_list)-1] = \t\t\t\t\t" + str(reversed_new_read_file_list[len(reversed_new_read_file_list)-1])

    if new_read_file_list[len(new_read_file_list)-1] == reversed_new_read_file_list[0]:
        print "[   "+CGREEN+"OK"+CEND+"   ] last went to first"
    else:
        print "[  "+CRED+"FAIL"+CEND+"  ] lsst does NOT equal first"
        print "new_read_file_list[len(new_read_file_list)-1] = \t\t\t\t\t" + str(new_read_file_list[len(new_read_file_list)-1])
        print "reversed_new_read_file_list[0] = \t\t\t\t\t" + str(reversed_new_read_file_list[0])

    print "End\t" + os.path.basename(__file__)
