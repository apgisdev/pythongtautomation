# master stops to gtfs stops
# desc: take the resulting master stops csv and build the "stops.txt" file
#   google needs to build a gtfs feed

import csv as cv

def index_stops_to_google_stops():

    f = open("MasterIndexToStops/stops_master_index_csv.txt", "rU")

    w = open("ActiveGoogleTransitSubmission/stops.txt", "w")

    w.write("stop_id,stop_name,stop_lat,stop_lon\n")

    list = []

    csvFile = cv.reader(f,delimiter=',', quotechar='\n')
    for row in csvFile:
        list.append(row)

    # print "list = ", str(list)
    # cut the top "lableing column line" off the top of the list
    list.pop(0)

    # positions - stop_id,stop_name,stop_lat,stop_lon
    position_stop_id = 0
    position_stop_name = 5
    position_stop_lat = -1
    position_stop_lon = -1

    for i in range(0, len(list)):

        # get the unique stop_id which indicates a point on the map
        # NOTE: stops may or may NOT have a timed
        stop_id = "S" + str(int(list[i][ position_stop_id ]) + 1)

        # get the stop name. EX: "2133 Memorial Dr. & Madison St."
        stop_name = str(list[i][ position_stop_name ])

        # get the latitude of the stop
        position_stop_lat = len(list[i]) - 4 # -87
        stop_lat = list[i][ position_stop_lat ]

        # get the longitude of the stop
        position_stop_lon = len(list[i]) - 3 # 32
        stop_lon = list[i][ position_stop_lon ]

        # assemble the line to write to the file
        theLine = str(stop_id) + "," + str(stop_name) + "," + str(stop_lon) + "," + str(stop_lat) + "\n"

        # write the line
        w.write(theLine)

if __name__ == "__main__":
    print "Begin index_stops_to_google_stops()"
    index_stops_to_google_stops()
    print "End index_stops_to_google_stops()"
