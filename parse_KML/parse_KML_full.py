import xml.etree.ElementTree as ET
import os
# ==============================================================================
# ================================== CLASSES ===================================
# ==============================================================================

class Polyline:
    points = None
    start_point = None
    end_point = None

    # CONSTRUCTOR

    def __init__(self, points):
        self.points = points
        # slice any leading empty points
        while(self.points[0] == ''):
            self.points = self.points[1:]
        # print "self.points = ", self.points
        self.start_point = self.points[0]
        self.end_point = self.points[len(self.points)-1]

    # METHODS

    def merge(self, other_points):
        # print "self.points = ",self.points
        # print "other_points = ",other_points
        self.points = self.points + other_points[1:]

        # reset the end point for the points
        self.end_point = self.points[len(self.points)-1]

    def append(self, points_set):
        self.points.append(points_set)

    def get_point(self, index):
        return self.points[index]

    # GETTERS AND SETTERS

    def get_points(self):
        return self.points
    def set_points(self, points):
        self.points = points
        self.start_point = points[0]
        self.end_point = points[len(points)-1]

    def get_start_point(self):
        return self.start_point
    def get_end_point(self):
        return self.end_point

# ==============================================================================
# ================================== FUNCTIONS =================================
# ==============================================================================

def getXMLTextFromPath(root, path, ns, tupleSet):
    # convert the string into an array
    arr = path.split("/")

    # call recursiveXMLfind with
    # * root is the base element of the kml
    # * arr is the xml-path of tags split into indiviual string of each tag
    # * ns is the namespace of the xml sheet
    # * textArr contains the text of the leaf xml nodes
    return recursiveXMLfind(root, arr, ns, tupleSet, tupleSet[0][0])

def recursiveXMLfind(root, arr, ns, tupleSet, name):

    # exit condition = if the path of the array is 1 (leaf of XML), then return
    if len(arr) < 2:
        return tupleSet

    # # cut the array first element
    arr = arr[1:]
    # print "arr = ", arr

    # get first element
    tag = arr[0]

    # for loop to find the tag
    for child in root:
        try:
            # print "child.attrib = '", child.attrib["id"],"'"
            id = child.attrib["id"]
            if child.tag == (ns + "Placemark"):
                for grandchild in child:
                    if grandchild.tag == (ns + "name"):
                        name = grandchild.text + "_" + id
        except KeyError:
            pass
        if child.tag == (ns + tag):
            # if we are at the leaf nodes of the xml tree
            if len(arr) <= 1:
                # append the text to the text-array
                # textArr.append(child.text.replace("  ","").replace("\n"," ").replace(",0","").split(" "))
                pushToTuple(tupleSet, str(name), child.text.replace("  ","").replace("\n"," ").replace(",0","").split(" "))
            # call the procedure again with:
            # * the child as the new root
            # * arr has it's top element removed
            # * ns is the xml namespace
            # * textArr is the array being built
            recursiveXMLfind(child, arr, ns, tupleSet, name)

    # return the built textArr containing the text from the KML file
    return tupleSet

def recursive_xml_placemark_id(root, arr, ns, id_arr):
    # exit condition = if the path of the array is 1 (leaf of XML), then return
    if len(arr) < 2:
        return id_arr

    # # cut the array first element
    arr = arr[1:]
    # print "arr = ", arr

    # get first element
    tag = arr[0]

    # for loop to find the tag
    for child in root:
        try:
            # print "child.attrib = '", child.attrib["id"],"'"
            if child.tag == (ns + "Placemark"):
                id = child.attrib["id"]
                for grandchild in child:
                    if grandchild.tag == (ns + "name"):
                        id_arr.append(grandchild.text + "_" + id)
        except KeyError:
            print "NO'"
        recursive_xml_placemark_id(child, arr, ns, id_arr)

    return id_arr

def get_xml_placemark_id(root, path, ns, id_arr):
    # convert the string into an array
    arr = path.split("/")

    # call recursiveXMLfind with
    # * root is the base element of the kml
    # * arr is the xml-path of tags split into indiviual string of each tag
    # * ns is the namespace of the xml sheet
    # * textArr contains the text of the leaf xml nodes
    return recursive_xml_placemark_id(root, arr, ns, id_arr)

def pushToTuple(tupleSet,key,valueToPush):
    position = -1
    for i in range(0,len(tupleSet)):
        if key == tupleSet[i][0]:
            position = i
            break
    # print "tupleSet["+str(position)+"][1] = ", tupleSet[position][1]
    # print "position] = ", position
    if position == -1:
        raise
    # print "valueToPush = ", valueToPush
    poly = Polyline(valueToPush)
    # print "poly = ", poly
    tupleSet[position][1].append(poly)
    return tupleSet

# ==============================================================================
# ==================================== MAIN ====================================
# ==============================================================================

if __name__ == "__main__":
    tree = ET.parse('doc 3.kml')
    root = tree.getroot()
    # print "root = ", str(root)

    namespace = "{http://www.opengis.net/kml/2.2}"

    # root -> Document -> Folder -> Placemark -> MultiGeometry -> LineString -> coordinates

    coordinates = []
    textArr = []
    polylines = []
    id_arr = []


    # text = getXMLTextFromPath(root, "kml/Document/Folder/Placemark/MultiGeometry/LineString/coordinates", namespace, textArr)
    placemarkNames = get_xml_placemark_id(root, "kml/Document/Folder/Placemark/MultiGeometry/LineString/coordinates", namespace, id_arr)

    print "placemarkNames = ",placemarkNames
    print "len(placemarkNames) = ",len(placemarkNames)

    # building the ids into tuples with associated arrays next to them
    tupleSet = []
    for i in range(0,len(placemarkNames)):
        tupleSet.append( (placemarkNames[i],[]) )

    print "tupleSet = ",tupleSet
    print "len(tupleSet) = ",len(tupleSet)

    tupleSet = getXMLTextFromPath(root, "kml/Document/Folder/Placemark/MultiGeometry/LineString/coordinates", namespace, tupleSet)

    # print "text = ",text
    print "len(tupleSet) = ",len(tupleSet)
    print "tupleSet= ",tupleSet
    print ""

    # print "tupleSet[0][1][0].get_points() = ", tupleSet[0][1][0].get_points()

    for i in range(0,len(tupleSet)):
        print "i= ",i,", tupleSet = ",tupleSet[i]

    # for all routes
    for i in range(0,len(tupleSet)):
        print "i= ",i

        # for all polylines in the route
        polyline_list = tupleSet[i][1]
        # Merge
        first_polyline = tupleSet[i][1][0]
        end = first_polyline.get_end_point()
        num_merges = 0
        loops = 0

        while ( num_merges < ( len(tupleSet[i][1]) - 1 ) and loops <= len(tupleSet[i][1]) ):
            # while
            # print "while"
            loops = loops + 1
            print "loops = ", loops
            for y in range(1,len(tupleSet[i][1])):
                # candidate polyline
                polyline = tupleSet[i][1][y]
                start = polyline.get_start_point()
                # print "start = ",start
                # print "end = ",end
                if start == end:
                    # print "merge"
                    first_polyline.merge(polyline.get_points())
                    end = first_polyline.get_end_point()
                    num_merges = num_merges + 1

    print "tupleSet[0][1][0] = ", tupleSet[0][1][0].get_points()

    # write out to a set of files
    # one file for each entry in tupleSet
    for i in range(0,len(tupleSet)):
        # replace any spaces for convenience
        # replace any
        f = open(""+str(tupleSet[i][0]).replace(" ","_").replace("/","")+".dat", "w+")
        for i in tupleSet[i][1][0].get_points():
            f.write(i.replace(","," ") + "\n")
        f.close()
