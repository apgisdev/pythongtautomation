# Google Transit Automation

This repository is designed to automate the building of the Google Transit Feed for the Clarksville Transit System. These tools should be considered incomplete and need additional work; however, I am confident they are useful.

## buildStopTimes.py
---

### Description
  A python program to build trips under the GTFS rule set

### Usage

To run the program and simply build all available routes known to the program run:

```
python buildStopTimes.py -a
```

### Optional Command Arguments

`-r <route-number>` - build a specific route
  * available: `1_FL,1,2,3,4,,5,6,7,8,8_7pm,8_8pm,_Industrial_Park,5_520_and_620,5_last_run,5_v2`

`-a` - build all available routes

`-z` - send all routes to a zip-file

`-v` - verbose, print all timed stops and (optional) zip-file files

### Details about buildStopTimes

Build GTFS trip data out of stop data (all and route)

* Takes in a file containing the stops times for the route: Rt<number>\_common_stop_times.txt
* Takes in a file containing the master index of all stop in the transit system. "result_stops_master_index_csv.txt"
* Takes in the stops made along the bus trip: Rt<number>\_common_out_in_stops_csv.txt
* Writes out the full trips to a file: result_Rt<number>\_common_out_in_stops_csv.txt
* Populate list filled with the lines of the input files
* Run through the routed stops and the master stops to find exact coordinate matches, then adds it to another list.
* Run though the new list building each line of the trip for all trips
  * if the program sees a "1" indicating the stop needs a time associated with it, grab the arrive and depart time out of the time "stop_times" list.

## buildShapeFile.py
---

Build a GTFS shape files out of a tortured KML file

* Open a file containing the points of a shape file
* For each line in the file prepend a "Sh<number>" string and append an increasing integer to the back of the line, then write the line

## RereverseList/reverseList.py
---

Reverses data in one file and outputs it. Used to reverse the order coordinates appear in a text file.

## Route_Industrial_Park/industrailPark.py
---

Build GTFS trip data out hard coded strings

* Go through the on-hour trips setting the arrival time, depart time, stod_id, and number in the sequence (stop_order).
* Repeat for the off hour trips. Except now one of the times is in the previous hour.

---

---

# Details

* Built using [Python 2.7](https://www.python.org/downloads/) and [ArcMap](http://desktop.arcgis.com/en/arcmap/)

# Contributors

* Harrison Welch - Programming, ArcMap
* Mason Cordell - Programming, ArcMap
* David Woods - ArcMap
* Michael Timbes - Programming
* Glenn Matthews - ArcMap
