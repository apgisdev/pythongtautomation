# find errors in the master stops file

import csv as cv
import re

if __name__ == "__main__":

    f = open("MasterIndexToStops/stops_master_index_csv.txt", "rU")

    w = open("master_stop_errors.txt", "w")

    w.write("stop_id,stop_name,stop_lat,stop_lon\n")

    list = []

    csvFile = cv.reader(f,delimiter=',', quotechar='\n')
    for row in csvFile:
        list.append(row)

    # Inspired by: https://stackoverflow.com/questions/3040716/python-elegant-way-to-check-if-at-least-one-regex-in-list-matches-a-string
    typos = [
        "Null",
        "Memoiral",
        "madison",
        "Tansit",
        "Trainsit",
        "Pradise",
        "Pardise",
        "Dumbar",
        "Wilam",
        "wilma",
        "Willma",
        "Wilmal",
        "Ruldolph",
        "Ruldolph",
        "Plazy",
        "LMadison",
        "Madion",
        "Tansit"
        "Wal-Mart",
        "WIlma",
        "Frnaklin",
        "Moutain",
        "Whitifield",
        "Pky",
        "Stonecorssing",
        "Providenve",
        "Prvidence",
        "Charlemage",
        "Layafette",
        "Charlemange",
        "Dupius",
        "blvd",
        "Pembrook Rd.",
        "Maket",
        "Marioin",
        "Cener",
        "Campblell",
        "Compbell",
        "Cmapbell",
        "Re\."
    ]

    missing_letters = [
        "Airborn",
        "Blv"
    ]

    combined = "(" + ")|(".join(typos) + ")|(.+" + ".+)|(.+".join(typos) + ".+)|(" + ")|(".join(missing_letters) + ")"
    # combined = "101st Airborn Division Pky & Pollard Rd./ Whitifield Rd."

    print "combined = ", combined
    position_stop_name = 5
    for i in range(0, len(list)):
        # get the stop name. EX: "2133 Memorial Dr. & Madison St."
        stop_name = str(list[i][ position_stop_name ])

        match = re.match(combined, stop_name)
            # print "stopname = ", stop_name
        if match:
            print "match = " + str(match.group(0))
            w.write(",".join(list[i]) + "\n")
        # else:
            # print "stopname = ", stop_name
