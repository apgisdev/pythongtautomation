# csv stops to master index stops
# ------------------------------------------------------------------------------
# desc: take the csv from ArcMap and mangle it into an index used by build

from tokenize import tokenize
import csv as cv

def arcmap_stops_to_index_stops():

    # readFile = open('workfile_route_1_merge_export.txt','r');

    filename = 'stops_master_index_csv.txt'

    table = []
    if __name__ == "__main__":
        readFile = open(filename,'rU')
    else:
        readFile = open("MasterIndexToStops/"+filename,"rU")

    # readFile = open('route1_outbound_first_last.csv','rU')

    csvFile = cv.reader(readFile,delimiter=',', quotechar='\n')

    for row in csvFile:
        # print str(row) + str(len(row))
        table.append(row)

    if __name__ == "__main__":
        writeFile = open('result_'+filename,'w')
    else:
        writeFile = open("MasterIndexToStops/"+'result_'+filename,'w')

    # cut the top "lableing column line" off the top of the list
    table.pop(0)

    # We need:
    #   1. stop_id    - unique identification
    #   2. stop_name  - name of the stop a bus can make (with sign, bench, ...)
    #   3. stop_lat   - degrees latitude on the globe   ( ~ 36.529...)
    #   4. stop_lon   - degrees longitude on the globe  ( ~ -87.259...)

    stop_name_position = 5      # 6th entry from the front of the row
    stop_lat_position_offset = 3
    stop_lon_position_offset = 4
    is_timed_offset = 1

    for x in range(0,len(table)):

        # stop_id
        stop_id = "S" + str(x+1)

        # stop_name
        stop_name = table[ x ][ stop_name_position ]
        # print "table[x] = ", table[x]

        # lat of the current line
        stop_lat_position = len(table[x]) - stop_lat_position_offset
        lat = table[ x ][ stop_lat_position ]

        # lon of the current line
        stop_lon_position = len(table[x]) - stop_lon_position_offset
        lon = table[ x ][ stop_lon_position ]

        isTimedStop = table[ x ][ len(table[x]) - is_timed_offset ]

        # combine all into a single line
        theLine = stop_id + "," + stop_name + "," + lat + "," + lon + "," + isTimedStop

        # print theLine

        # write to the file the line plus a newline
        writeFile.write(theLine + "\n");

if __name__ == "__main__":
    print "Hello World"

    # go
    arcmap_stops_to_index_stops()
    print "Done"
