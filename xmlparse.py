import xml.etree.ElementTree as ET

# ==============================================================================
# ================================== CLASSES ===================================
# ==============================================================================

class Polyline:
    points = None
    start_point = None
    end_point = None

    # CONSTRUCTOR

    def __init__(self, points):
        self.points = points
        # print "self.points = ", self.points
        self.start_point = points[0]
        self.end_point = points[len(points)-1]

    # METHODS

    def merge(self, other_points):
        arr = []
        print "self.points = ",self.points
        print "other_points = ",other_points
        arr = arr + self.points[:1] + other_points
        self.points = arr

    # GETTERS AND SETTERS

    def get_points(self):
        return self.points
    def set_points(self, points):
        self.points = points
        self.start_point = points[0]
        self.end_point = points[len(points)-1]

    def get_start_point(self):
        return self.start_point
    def get_end_point(self):
        return self.end_point

# ==============================================================================
# ================================== FUNCTIONS =================================
# ==============================================================================

def getXMLTextFromPath(root, path, ns, textArr):
    # convert the string into an array
    arr = path.split("/")

    # call recursiveXMLfind with
    # * root is the base element of the kml
    # * arr is the xml-path of tags split into indiviual string of each tag
    # * ns is the namespace of the xml sheet
    # * textArr contains the text of the leaf xml nodes
    return recursiveXMLfind(root, arr, ns, textArr);

def recursiveXMLfind(root, arr, ns, textArr):

    # exit condition = if the path of the array is 1 (leaf of XML), then return
    if len(arr) < 2:
        return []

    # # cut the array first element
    arr = arr[1:]
    # print "arr = ", arr

    # get first element
    tag = arr[0]

    # for loop to find the tag
    for child in root:
        if child.tag == (ns + tag):
            # if we are at the leaf nodes of the xml tree
            if len(arr) <= 1:
                # append the text to the text-array
                textArr.append(child.text.replace("  ","").replace("\n"," ").replace(",0","").split(" "))
            # call the procedure again with:
            # * the child as the new root
            # * arr has it's top element removed
            # * ns is the xml namespace
            # * textArr is the array being built
            recursiveXMLfind(child, arr, ns, textArr)

    # return the built textArr containing the text from the KML file
    return textArr

# ==============================================================================
# ==================================== MAIN ====================================
# ==============================================================================

if __name__ == "__main__":
    # tree = ET.parse('doc 2.kml')
    tree = ET.parse('r8.kml')
    root = tree.getroot()
    # print "root = ", str(root)

    namespace = "{http://www.opengis.net/kml/2.2}"

    # root -> Document -> Folder -> Placemark -> MultiGeometry -> LineString -> coordinates

    coordinates = []
    textArr = []
    polylines = []

    text = getXMLTextFromPath(root, "kml/Document/Folder/Placemark/MultiGeometry/LineString/coordinates", namespace, textArr)

    # print "text = ",text
    # print "len(text) = ",len(text)

    # write out to a test file
    # f = open('workfile.txt', 'w')
    x = 0
    for i in text:
        # print "i = ", i
        if i[0] == '':
            i = i[1:]
        polylines.append(Polyline(i))
    # for i in range(0,len(text)):
        # print "polylines[i].get_start_point = ", polylines[i].get_start_point()

    # print "polylines = ", polylines

    # merge the polylines up into one big polylines
    # ASSUME the first point of the first set of points

    polyline = []
    end_point = None
    start_point = None

    polyline = polyline + polylines[0].points

    print "polylines[0].points = ", polylines[0].points
    print "==========================================="

    for i in range(0,len(polylines)):
        end_point = polyline[len(polyline)-1]
        print "end_point = ", end_point
        for j in range(1,len(polylines)):
            start_point = polylines[j].get_start_point()
            # print "start_point = ", start_point

            if end_point == start_point:
                print "TRUE"
                polyline = polyline[:-1] + polylines[j].points
                print "curr polyline = ", polyline
                # print "polylines[j].points = ", polylines[j].points
                # polyline = polyline + polylines[j].points
                # print "polylines[0].points = ", polylines[0].points
                # polylines.pop(j)

    print "polyline = ", polyline
    print "len(polyline) = ", len(polyline)

    # points = []
    # point = None;
    # for i in range(0,len(polyline)):
    #     point = polyline[i]
    #     point = point.split(",")
    #     points.append([point[1],point[0]])


    f = open('workfile3.txt', 'w')

    for i in polyline:
        f.write(i.replace(",", " ") + "\n")
