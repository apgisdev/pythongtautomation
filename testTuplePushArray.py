# tuple push text
def pushToTuple(tupleSet,key,valueToPush):
    position = -1
    for i in range(0,len(tupleSet)):
        if key == tupleSet[i][0]:
            position = i
            break
    print "tupleSet[position][1] = ", tupleSet[position][1]
    print "position] = ", position
    if position == -1:
        raise
    tupleSet[position][1].append(valueToPush)
    return tupleSet


tupleSet = [('name1',[1,2,3]),('name2',[4,5,6]),('name3',[7,8,9])]

print "tupleSet = ", tupleSet

tupleSet = pushToTuple(tupleSet,'name2',999)

print "tupleSet = ", tupleSet
